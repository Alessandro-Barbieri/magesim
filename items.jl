chest::DF.DataFrame = DF.DataFrame()
cloak::DF.DataFrame = DF.DataFrame()
feet::DF.DataFrame = DF.DataFrame()
finger::DF.DataFrame = DF.DataFrame()
hands::DF.DataFrame = DF.DataFrame()
head::DF.DataFrame = DF.DataFrame()
legs::DF.DataFrame = DF.DataFrame()
neck::DF.DataFrame = DF.DataFrame()
offhand::DF.DataFrame = DF.DataFrame()
onehanded::DF.DataFrame = DF.DataFrame()
ranged::DF.DataFrame = DF.DataFrame()
shoulder::DF.DataFrame = DF.DataFrame()
trinket::DF.DataFrame = DF.DataFrame()
trinket_both::DF.DataFrame = DF.DataFrame()
trinket_active::DF.DataFrame = DF.DataFrame()
twohanded::DF.DataFrame = DF.DataFrame()
waist::DF.DataFrame = DF.DataFrame()
weapon::DF.DataFrame = DF.DataFrame()
wrist::DF.DataFrame = DF.DataFrame()
ench_chest::DF.DataFrame = DF.DataFrame()
ench_hands::DF.DataFrame = DF.DataFrame()
ench_head::DF.DataFrame = DF.DataFrame()
ench_legs::DF.DataFrame = DF.DataFrame()
ench_shoulder::DF.DataFrame = DF.DataFrame()
ench_weapon::DF.DataFrame = DF.DataFrame()
ench_wrist::DF.DataFrame = DF.DataFrame()

#PROFESSIONS
append!(cols=:union,chest,DF.DataFrame(name = "Robe of the Archmage",int = 12,sp = 40,crit = 0.01,RotA=1))
append!(cols=:union,cloak,DF.DataFrame(name="Hide of the Wild",int=10))
append!(cols=:union,hands,DF.DataFrame(name="Gloves of Spell Mastery",int=10,spi=8,crit=0.02))
append!(cols=:union,waist,DF.DataFrame(name="Belt of the Archmage",int=25,crit=0.01))
append!(cols=:union,onehanded,DF.DataFrame(name="Sageblade",int=6,sp=20))

#DARKMOON FAIRE
append!(cols=:union,neck,DF.DataFrame(name="Orb of the Darkmoon",spi=8,sp=22))
append!(cols=:union,trinket,DF.DataFrame(name="Darkmoon Card: Blue Dragon",DCBD=1))

#SILITHUS
append!(cols=:union,cloak,DF.DataFrame(name="Windshear Cape",int=7))
append!(cols=:union,finger,DF.DataFrame(name="Elemental Focus Band", sp=22,int=11))
append!(cols=:union,finger,DF.DataFrame(name="Wrath of Cenarius",WoC=1)) #
append!(cols=:union,neck,DF.DataFrame(name="Wavefront Necklace",int=6,mp5=8))
append!(cols=:union,wrist,DF.DataFrame(name="Rockfury Bracers",sp=27,hit=0.01)) #
append!(cols=:union,onehanded,DF.DataFrame(name="Elemental Attuned Blade",int=6,sp=32,mp5=3))
append!(cols=:union,offhand,DF.DataFrame(name="Earthcalm Orb",int=10,mp5=8))

#PVP
append!(cols=:union,chest,DF.DataFrame(name="Warlord's Silk Raiment",spi=6,int=17,sp=33,crit=0.01,PVP=1))
append!(cols=:union,feet,DF.DataFrame(name="General's Silk Boots",int=14,spi=6,PVP=1,sp=21,hit=0.01))
append!(cols=:union,finger,DF.DataFrame(name="Don Rodrigos's Band", crit=0.01))
append!(cols=:union,hands,DF.DataFrame(name="General's Silk Handguards",int=12,spi=5,sp=27,PVP=1))
append!(cols=:union,head,DF.DataFrame(name="Warlord's Silk Cowl",int=17,spi=6,crit=0.01,sp=33,PVP=1))
append!(cols=:union,legs,DF.DataFrame(name="General's Silk Trousers",int=20,spi=10,sp=30,PVP=1,crit=0.01))
append!(cols=:union,legs,DF.DataFrame(name="Outrider's Silk Leggings",int=19,spi=10,sp=28))
append!(cols=:union,shoulder,DF.DataFrame(name="Warlord's Silk Amice",int=15,spi=5,sp=25,PVP=1))
append!(cols=:union,shoulder,DF.DataFrame(name="Defiler's Epaulets",int=17,sp=12,mp5=4))
append!(cols=:union,twohanded,DF.DataFrame(name="High Warlord's War Staff",int=23,spi=17,sp=71))
append!(cols=:union,twohanded,DF.DataFrame(name="Ironbark Staff",int=10,sp=41,crit=0.01))
append!(cols=:union,onehanded,DF.DataFrame(name="High Warlord's Spellblade",int=8,sp=72,crit=0.01))
append!(cols=:union,offhand,DF.DataFrame(name="High Warlord's Tome of Destruction",int=16,sp=20))
append!(cols=:union,offhand,DF.DataFrame(name="Therazane's Touch",sp=33))
append!(cols=:union,offhand,DF.DataFrame(name="Mindfang",sp=30,crit=0.01))
append!(cols=:union,offhand,DF.DataFrame(name="Tome of the Ice Lord",frostsp=34,int=9))
append!(cols=:union,offhand,DF.DataFrame(name="Tome of Fiery Arcana",firesp=40))
append!(cols=:union,offhand,DF.DataFrame(name="Tome of Arcane Domination",mp5=3))
append!(cols=:union,offhand,DF.DataFrame(name="Lei of the Lifegiver",mp5=3))
append!(cols=:union,waist,DF.DataFrame(name="General's Silk Sash",int=23,spi=15))
append!(cols=:union,wrist,DF.DataFrame(name="General's Silk Cuffs",int=19,spi=10))

#WORLD BOE
append!(cols=:union,chest,DF.DataFrame(name="Alanna's Embrace",int=20,spi=20,sp=20))
append!(cols=:union,chest,DF.DataFrame(name="Robes of Insight",int=25,spi=15,RoI=1))
append!(cols=:union,cloak,DF.DataFrame(name="Energy Cloak",spi=10,EC=1))
append!(cols=:union,finger,DF.DataFrame(name="Underworld Band", spi=6))
append!(cols=:union,legs,DF.DataFrame(name="Master's Leggings of Spirit",spi=29)) #
append!(cols=:union,neck,DF.DataFrame(name="Lady Maye's pendant",spi=10,int=19))
append!(cols=:union,neck,DF.DataFrame(name="Lei of Lilies",spi=15)) #
append!(cols=:union,neck,DF.DataFrame(name="Jeweled Amulet of Cainwyn",spi=10,int=18))
append!(cols=:union,twohanded,DF.DataFrame(name="Staff of Jordan",int=11,spi=11,sp=26))
append!(cols=:union,twohanded,DF.DataFrame(name="Glowing Brightwood Staff",int=29,spi=9))
append!(cols=:union,twohanded,DF.DataFrame(name="Elemental Mage Staff",frostsp=36,firesp=36,frostr=20)) #
append!(cols=:union,wrist,DF.DataFrame(name="Master's Bracers of Spirit",spi=15)) #

#INSTANCE
append!(cols=:union,chest,DF.DataFrame(name="Vestments of the Atal'ai Prophet",int=11,spi=27)) #
append!(cols=:union,chest,DF.DataFrame(name="Mindsurge Robe",mp5=10)) #
append!(cols=:union,cloak,DF.DataFrame(name="Archivist Cape of Spirit",spi=15,mp5=4)) #
append!(cols=:union,feet,DF.DataFrame(name="The Postmaster's Treads",int=15,spi=6,sp=7,postmaster=1)) #
append!(cols=:union,head,DF.DataFrame(name="The Postmaster's Band",int=25,spi=10,sp=14,postmaster=1)) #
append!(cols=:union,chest,DF.DataFrame(name="The Postmaster's Tunic",int=20,spi=10,sp=15,postmaster=1)) #
append!(cols=:union,legs,DF.DataFrame(name="The Postmaster's Trousers",int=20,spi=20,postmaster=1)) #
append!(cols=:union,finger,DF.DataFrame(name="The Postmaster's Seal",int=3,spi=17,postmaster=1)) #
append!(cols=:union,head,DF.DataFrame(name="Crown of Caer Darrow",int=20,spi=20,frostr=15)) #
append!(cols=:union,legs,DF.DataFrame(name="Haunting Specter Leggings",int=12,spi=28)) #
append!(cols=:union,shoulder,DF.DataFrame(name="Diabolic Mantle",mp5=8))
append!(cols=:union,trinket,DF.DataFrame(name="Eye of the Beast",crit=0.02)) #
append!(cols=:union,trinket,DF.DataFrame(name="Briarwood Reed",sp=29))
append!(cols=:union,trinket,DF.DataFrame(name="Royal Seal of Eldre'Thalas",mp5=8)) #
append!(cols=:union,trinket,DF.DataFrame(name="Shard of the Splithooves",mp5=5))
append!(cols=:union,trinket,DF.DataFrame(name="Mindtap Talisman",mp5=11)) #
append!(cols=:union,trinket_active,DF.DataFrame(name="Burst of Knowledge",sp=15,cooldown=15*60,duration=10,type=:BoK)) #
append!(cols=:union,trinket_active,DF.DataFrame(name="Fire Ruby",cooldown=3*60,duration=60,type=:FR))
append!(cols=:union,wrist,DF.DataFrame(name="Wyrmthalak's Shackles",int=9,spi=15))

#ARGENT DAWN
append!(cols=:union,neck,DF.DataFrame(name="Amulet of the Dawn",int=13,sp=15))
append!(cols=:union,wrist,DF.DataFrame(name="Bracers of Hope",int=11,spi=12,sp=18))
append!(cols=:union,trinket_active,DF.DataFrame(name="Talisman of Ascendance",cooldown=60,duration=20,type=:ToA))

#TIER 0
append!(cols=:union,chest,DF.DataFrame(name="Magister's Robes",int=31,spi=8,T0=1))
append!(cols=:union,feet,DF.DataFrame(name="Magister's Boots",int=14,spi=14,T0=1))
append!(cols=:union,hands,DF.DataFrame(name = "Magister's Gloves",T0=1,spi=14,int=14))
append!(cols=:union,head,DF.DataFrame(name = "Magister's Crown",T0=1,spi=5,int=30))
append!(cols=:union,legs,DF.DataFrame(name = "Magister's Leggings",T0=1,spi=21,int=20))
append!(cols=:union,shoulder,DF.DataFrame(name = "Magister's Mantle",T0=1,spi=6,int=22))
append!(cols=:union,waist,DF.DataFrame(name="Magister's Belt",int=21,spi=6,T0=1))
append!(cols=:union,wrist,DF.DataFrame(name="Magister's Bindings",int=15,spi=5,T0=1))

#TIER 0.5
append!(cols=:union,chest,DF.DataFrame(name="Sorcerer's Robes",int=25,spi=9,sp=16,T0_5=1)) #
append!(cols=:union,feet,DF.DataFrame(name="Sorcerer's Boots",int=16,spi=10,sp=21,T0_5=1)) #
append!(cols=:union,hands,DF.DataFrame(name = "Sorcerer's Gloves",T0_5=1,spi=10,int=14,sp=12,hit=0.01)) #
append!(cols=:union,head,DF.DataFrame(name = "Sorcerer's Crown",T0_5=1,spi=14,int=25,sp=11,crit=0.01)) #
append!(cols=:union,legs,DF.DataFrame(name = "Sorcerer's Leggings",T0_5=1,spi=10,int=22,sp=16)) #
append!(cols=:union,shoulder,DF.DataFrame(name = "Sorcerer's Mantle",T0_5=1,spi=7,int=17,sp=9)) #
append!(cols=:union,waist,DF.DataFrame(name="Sorcerer's Belt",int=14,spi=7,sp=14,T0_5=1)) #
append!(cols=:union,wrist,DF.DataFrame(name="Sorcerer's Bindings",int=12,spi=5,sp=8,T0_5=1)) #

#INSTANCE EPIC
append!(cols=:union,chest,DF.DataFrame(name="Embrace of the Wind Serpent",spi=30,int=17))
##append!(cols=:union,head,DF.DataFrame(name="Cap of the Scarlet Savant",int=20,crit=0.02))
append!(cols=:union,twohanded,DF.DataFrame(name="Headmaster's Charge",int=20))
append!(cols=:union,onehanded,DF.DataFrame(name="Blade of the Eternal Darkness",BoED=1))
append!(cols=:union,offhand,DF.DataFrame(name="Book of the Dead",int=5,spi=10)) #

#WORLD BOSSES
append!(cols=:union,chest,DF.DataFrame(name="Jade Inlaid Vestments",int=18,spi=8,sp=44))
append!(cols=:union,cloak,DF.DataFrame(name="Drape of Benediction",int=13,spi=8))
append!(cols=:union,feet,DF.DataFrame(name="Mendicant's Slippers",int=23,mp5=10))
append!(cols=:union,feet,DF.DataFrame(name="Snowblind Shoes",int=10,sp=32,mp5=5))
append!(cols=:union,finger,DF.DataFrame(name="Ring of Entropy", int=13,spi=8, crit=0.01))
append!(cols=:union,finger,DF.DataFrame(name="Mindtear Band",sp=21,int=6,crit=0.01))
append!(cols=:union,hands,DF.DataFrame(name="Gloves of Delusional Power",int=16,mp5=5,sp=27))
##append!(cols=:union,head,DF.DataFrame(name="Crystal Adorned Crown",int=14,spi=13))
append!(cols=:union,neck,DF.DataFrame(name="Dragonheart Necklace",int=6,spi=6))
append!(cols=:union,twohanded,DF.DataFrame(name="Amberseal Keeper",int=20,sp=44,mp5=12,frostr=5,shadowr=5))
append!(cols=:union,twohanded,DF.DataFrame(name="Staff of Rampant Growth",int=16,mp5=11))
append!(cols=:union,onehanded,DF.DataFrame(name="Fang of the Mystics",int=10,sp=40,mp5=4,crit=0.01))
append!(cols=:union,offhand,DF.DataFrame(name="Trance Stone",int=8,spi=8,sp=25))
append!(cols=:union,waist,DF.DataFrame(name="Belt of the Dark Bog",int=8,sp=14))
append!(cols=:union,wrist,DF.DataFrame(name="Blacklight Bracer",int=13,spi=8,crit=0.01))
append!(cols=:union,wrist,DF.DataFrame(name="Dryad's Wrist Bindings",int=8,spi=7,sp=22))
append!(cols=:union,wrist,DF.DataFrame(name="Black Bark Wristbands",int=4,spi=4,sp=25))

#AHN'QIRAJ OPENING
append!(cols=:union,head,DF.DataFrame(name="Gnomish Turban of Psychic Might",int=31,mp5=9)) #
append!(cols=:union,neck,DF.DataFrame(name="Drake Tooth Necklace",int=12))
append!(cols=:union,onehanded,DF.DataFrame(name="Runesword of the Red",int=9,spi=7,sp=64))
append!(cols=:union,onehanded,DF.DataFrame(name="Fang of Korialstrasz",int=14,spi=13))

#ONYXIA'S LAIR
append!(cols=:union,finger,DF.DataFrame(name="Dragonslayer's Signet", int=12,spi=6, crit=0.01)) #
append!(cols=:union,head,DF.DataFrame(name="Netherwind Crown",spi=7,int=26,mp5=4,sp=32,T2=1,frostr=10,shadowr=10)) #
append!(cols=:union,trinket,DF.DataFrame(name="Shard of the Scale",mp5=16))
append!(cols=:union,offhand,DF.DataFrame(name="Ancient Cornerstone Grimoire",int=15,spi=11))

#MOLTEN CORE
append!(cols=:union,chest,DF.DataFrame(name="Arcanist Robes",spi=10,int=25,sp=23,T1=1)) #
append!(cols=:union,chest,DF.DataFrame(name="Robe of Volatile Power",int=15,spi=10,crit=0.02,sp=23))
append!(cols=:union,chest,DF.DataFrame(name="Flarecore Robe",sp=23))
append!(cols=:union,cloak,DF.DataFrame(name="Fireproof Cloak",int=9,spi=8))
append!(cols=:union,feet,DF.DataFrame(name="Arcanist Boots",spi=11,int=14,T1=1,sp=11,crit=0.01,shadowr=10)) #
append!(cols=:union,finger,DF.DataFrame(name="Seal of the Archmagus", int=11, spi=11, mp5=3,frostr=6,shadowr=6)) #
append!(cols=:union,finger,DF.DataFrame(name="Band of Sulfuras", int=23,spi=10))
append!(cols=:union,finger,DF.DataFrame(name = "Ring of Spell Power", sp=33)) #
append!(cols=:union,finger,DF.DataFrame(name="Cauterizing Band", int=12))
append!(cols=:union,hands,DF.DataFrame(name="Arcanist Gloves",T1=1,spi=10,int=15,sp=14,mp5=4)) #
append!(cols=:union,hands,DF.DataFrame(name="Gloves of the Hypnotic Flame",int=19,spi=8,firesp=23,sp=9)) #
append!(cols=:union,hands,DF.DataFrame(name="Flarecore Gloves",int=14))
append!(cols=:union,head,DF.DataFrame(name="Arcanist Crown",int=27,spi=10,T1=1,sp=20,hit=0.01)) #
append!(cols=:union,legs,DF.DataFrame(name="Arcanist Leggings",T1=1,spi=10,int=23,crit=0.01,sp=20,shadowr=10)) #
append!(cols=:union,legs,DF.DataFrame(name="Manastorm Leggings",int=14,mp5=14))
append!(cols=:union,legs,DF.DataFrame(name="Netherwind Pants",T2=1,spi=5,int=27,sp=30,crit=0.01))
append!(cols=:union,neck,DF.DataFrame(name ="Choker of Enlightenment",int=10,spi=10,sp=18)) #
append!(cols=:union,neck,DF.DataFrame(name="Choker of the Fire Lord",int=7,sp=34))
append!(cols=:union,ranged,DF.DataFrame(name="Crimson Shocker",int=10))
append!(cols=:union,shoulder,DF.DataFrame(name="Arcanist Mantle",T1=1,spi=5,int=21,mp5=4,sp=14,shadowr=7)) #
##append!(cols=:union,shoulder,DF.DataFrame(name="Flarecore Mantle",spi=10,int=10))
append!(cols=:union,trinket_active,DF.DataFrame(name="Talisman of Ephemeral Power",cooldown=90,duration=15,type=:ToEP))
append!(cols=:union,twohanded,DF.DataFrame(name="Staff of Dominance",int=37,spi=14,crit=0.01,sp=40))
append!(cols=:union,onehanded,DF.DataFrame(name="Azuresong Mageblade",int=12,sp=40,crit=0.01))
append!(cols=:union,onehanded,DF.DataFrame(name="Sorcerous Dagger",int=17,sp=20))
append!(cols=:union,offhand,DF.DataFrame(name="Fire Runed Grimoire",int=21,sp=11))
append!(cols=:union,waist,DF.DataFrame(name ="Arcanist Belt",T1=1,int=20,spi=10,sp=14)) #
append!(cols=:union,waist,DF.DataFrame(name="Mana Igniting Cord",int=16,sp=25,crit=0.01)) #
append!(cols=:union,wrist,DF.DataFrame(name ="Arcanist Bindings",T1=1,int=15,spi=6,sp=12,mp5=3)) #
append!(cols=:union,wrist,DF.DataFrame(name="Flarecore Wraps",int=8,mp5=9))

#BLACKWING LAIR
append!(cols=:union,chest,DF.DataFrame(name="Netherwind Robes",spi=8,int=26,sp=32,crit=0.01,T2=1)) #
append!(cols=:union,chest,DF.DataFrame(name="Black Ash Robe",int=22,spi=17))
append!(cols=:union,cloak,DF.DataFrame(name="Cloak of the Brood Lord",int=14,sp=26))
append!(cols=:union,cloak,DF.DataFrame(name="Shroud of Pure Thought",int=11,mp5=6))
append!(cols=:union,feet,DF.DataFrame(name="Netherwind Boots", spi=10, int=16, sp=27,T2=1)) #
append!(cols=:union,feet,DF.DataFrame(name = "Ringo's Blizzard Boots", int=12, hit=0.01, frostsp=40)) #
append!(cols=:union,feet,DF.DataFrame(name="Shimmering Geta",int=17,mp5=12)) #
append!(cols=:union,feet,DF.DataFrame(name="Boots of Pure Thought",int=12,spi=12))
append!(cols=:union,finger,DF.DataFrame(name="Ring of Blackrock", sp=19, mp5=9)) #
append!(cols=:union,finger,DF.DataFrame(name="Band of Forced Concentration", sp=21,int=12,hit=0.01)) #
append!(cols=:union,finger,DF.DataFrame(name="Pure Elementium Band", int=10,spi=10))
append!(cols=:union,finger,DF.DataFrame(name="Band of Dark Dominion", int=6))
append!(cols=:union,hands,DF.DataFrame(name="Netherwind Gloves",T2=1,int=16,spi=6,sp=20,crit=0.01,shadowr=10)) #
append!(cols=:union,hands,DF.DataFrame(name="Gloves of Rapid Evolution",int=12,spi=32)) #
append!(cols=:union,hands,DF.DataFrame(name="Ebony Flame Gloves",int=12))
append!(cols=:union,head,DF.DataFrame(name = "Mish'undare, Circlet of the Mind Flayer",int=24,spi=9,sp=35,crit=0.02)) #
append!(cols=:union,legs,DF.DataFrame(name="Empowered Leggings",int=12,spi=24,crit=0.01))
append!(cols=:union,neck,DF.DataFrame(name="Pendant of the Fallen Dragon",int=12,mp5=9))
append!(cols=:union,ranged,DF.DataFrame(name="Essence Gatherer",int=7,mp5=5)) #
append!(cols=:union,ranged,DF.DataFrame(name="Dragon's Touch",int=12,sp=6))
append!(cols=:union,shoulder,DF.DataFrame(name ="Netherwind Mantle",T2=1,mp5=4,int=13,spi=12,sp=21)) #
append!(cols=:union,shoulder,DF.DataFrame(name="Mantle of the Blackwing Cabal",int=16,sp=34)) #
append!(cols=:union,trinket,DF.DataFrame(name="Neltharion's Tear",sp=44,hit=0.02)) #
append!(cols=:union,trinket_active,DF.DataFrame(name ="Mind Quickening Gem",cooldown=5*60,duration=20,type=:MQG)) #
append!(cols=:union,trinket,DF.DataFrame(name="Rejuvenating Gem",mp5=9))
append!(cols=:union,twohanded,DF.DataFrame(name="Shadow Wing Focus Staff",int=40,spi=17,sp=56))
append!(cols=:union,twohanded,DF.DataFrame(name="Staff of the Shadow Flame",int=29,spi=18,crit=0.02,sp=84))
append!(cols=:union,onehanded,DF.DataFrame(name="Claw of Chromaggus",int=17,sp=64,mp5=4))
append!(cols=:union,offhand,DF.DataFrame(name="Master Dragonslayer's Orb",int=14,sp=28))
append!(cols=:union,waist,DF.DataFrame(name="Netherwind Belt",T2=1,spi=13,int=20,sp=23,shadowr=10)) #
append!(cols=:union,waist,DF.DataFrame(name="Angelista's Grasp",spi=13,int=20,hit=0.02)) #
append!(cols=:union,waist,DF.DataFrame(name="Firemaw's Clutch",int=12,sp=35,mp5=5)) #
append!(cols=:union,wrist,DF.DataFrame(name="Netherwind Bindings",T2=1,spi=8,int=15,sp=19,mp5=4)) #
append!(cols=:union,wrist,DF.DataFrame(name="Bracers of Arcane Accuracy",int=12,hit=0.01,sp=21))

#ZUL'GURUB
append!(cols=:union,chest,DF.DataFrame(name="Zandalar Illusionist's Robe",int=24,sp=27,crit=0.01,ZG=1)) #
append!(cols=:union,chest,DF.DataFrame(name="Flowing Ritual Robes",int=23,spi=24,sp=22))
append!(cols=:union,cloak,DF.DataFrame(name="Cloak of Consumption",int=10,sp=23,hit=0.01))
append!(cols=:union,feet,DF.DataFrame(name="Betrayer's Boots",int=12,spi=12,sp=30))
append!(cols=:union,finger,DF.DataFrame(name="Band of Servitude", sp=23,int=9))
append!(cols=:union,neck,DF.DataFrame(name="Jewel of Kajaro",spi=8,int=13,sp=9,ZG=1)) #
append!(cols=:union,neck,DF.DataFrame(name="Soul Corrupter's Necklace",spi=8,int=16,hit=0.01)) #
append!(cols=:union,neck,DF.DataFrame(name="Jin'Do Evil Eye",spi=6,int=11))
append!(cols=:union,ranged,DF.DataFrame(name="Mar'li's Touch",int=11,spi=6))
append!(cols=:union,ranged,DF.DataFrame(name="Touch of Chaos",sp=18)) #
append!(cols=:union,shoulder,DF.DataFrame(name="Zandalar Illusionist's Mantle",ZG=1,int=21,spi=10,sp=12)) #
append!(cols=:union,trinket,DF.DataFrame(name="Hazza'rah Charm of Magic",ZG=1)) #
append!(cols=:union,trinket_active,DF.DataFrame(name="Mar'li's Eye",cooldown=3*60,duration=30,type=:MLE))
append!(cols=:union,trinket_active,DF.DataFrame(name="Zandalarian Hero Charm",cooldown=2*60,duration=20,type=:ZHC)) #
append!(cols=:union,twohanded,DF.DataFrame(name="Jin'Do's Judgement",int=10,mp5=14,sp=27,hit=0.01))
append!(cols=:union,twohanded,DF.DataFrame(name ="Will of Arlokk",spi=35,int=19)) #
append!(cols=:union,onehanded,DF.DataFrame(name="Bloodcaller",int=15,sp=33))
append!(cols=:union,onehanded,DF.DataFrame(name="Fang of Venoxis",int=8,spi=6,sp=24,mp5=6))
append!(cols=:union,offhand,DF.DataFrame(name="Jin'do Bag of Whammies",int=11,sp=18,hit=0.01))
append!(cols=:union,wrist,DF.DataFrame(name="Zandalar Illusionist's Wraps",ZG=1,spi=9,int=11,sp=14)) #

#RUINS OF AHN'QUIRAJ
append!(cols=:union,chest,DF.DataFrame(name="Vestments of the Shifting Sands",int=14,spi=8,sp=32,crit=0.01))
append!(cols=:union,cloak,DF.DataFrame(name="Drape of Vaulted Secrets",int=9,spi=6,AQ=1,sp=18)) #
append!(cols=:union,finger,DF.DataFrame(name="Band of Vaulted Secrets", sp=18, crit=0.01, AQ=1)) #
append!(cols=:union,hands,DF.DataFrame(name="Gloves of Dark Wisdom",int=20,mp5=5))
append!(cols=:union,head,DF.DataFrame(name="Dustwind Turban",int=29,spi=15,crit=0.01))
append!(cols=:union,legs,DF.DataFrame(name ="Leggings of the Black Blizzard",int=16,spi=8,sp=41,crit=0.01)) #
append!(cols=:union,neck,DF.DataFrame(name="Charm of the Shifting Sands",int=12,sp=25)) #
append!(cols=:union,trinket_active,DF.DataFrame(name="Eye of Moam",cooldown=3*60,duration=30,type=:EoM)) #
append!(cols=:union,offhand,DF.DataFrame(name="Talon of Furious Concentration",int=8,sp=21,crit=0.01))
append!(cols=:union,twohanded,DF.DataFrame(name="Staff of the Ruins",int=23,spi=14,sp=60,crit=0.01,hit=0.01))
append!(cols=:union,onehanded,DF.DataFrame(name="Blade of Vaulted Secrets",int=16,sp=40,hit=0.01))
append!(cols=:union,wrist,DF.DataFrame(name="Shackles of the Unscarred",int=12,sp=21)) #

#TEMPLE OF AHN'QUIRAJ
append!(cols=:union,chest,DF.DataFrame(name="Enigma Robes",sp=39,int=26,spi=8,crit=0.01,T2_5=1)) #
append!(cols=:union,chest,DF.DataFrame(name="Robes of the Guardian Saint",int=22,mp5=7))
append!(cols=:union,chest,DF.DataFrame(name="Robes of the Triumvirate",int=22,mp5=7))
append!(cols=:union,chest,DF.DataFrame(name="Garb of Royal Ascension",sp=30,hit=0.02,shadowr=25))
append!(cols=:union,chest,DF.DataFrame(name="Robes of the Battleguard",int=17,spi=8,sp=36))
append!(cols=:union,cloak,DF.DataFrame(name="Cloak of Clarity",int=12,spi=7,mp5=8))
append!(cols=:union,cloak,DF.DataFrame(name="Cloak of the Devoured",int=10,sp=30,hit=0.01)) #
append!(cols=:union,cloak,DF.DataFrame(name="Cape of the Trinity",int=12,sp=21))
append!(cols=:union,feet,DF.DataFrame(name="Enigma Boots",int=15,spi=6,sp=28,hit=0.01,mp5=4,T2_5=1))
append!(cols=:union,feet,DF.DataFrame(name="Boots of Epiphany",int=19,sp=34))
append!(cols=:union,feet,DF.DataFrame(name="Recomposed Boots",int=13,sp=20))
append!(cols=:union,finger,DF.DataFrame(name="Signet Ring of the Bronze Dragonflight", sp=28,int=9, mp5=5)) #
append!(cols=:union,finger,DF.DataFrame(name="Ring of the Fallen God", sp=37,int=6,hit=0.01)) #
append!(cols=:union,finger,DF.DataFrame(name="Ritssyn's Ring of Chaos", sp=25,crit=0.01))
append!(cols=:union,finger,DF.DataFrame(name="Ring of Swarming Thought", sp=26))
append!(cols=:union,finger,DF.DataFrame(name="Ring of the Devoured", mp5=8))
append!(cols=:union,finger,DF.DataFrame(name="Ring of the Martyr", mp5=5))
append!(cols=:union,hands,DF.DataFrame(name="Dark Storm Gauntlets",int=15,sp=37,hit=0.01)) #
append!(cols=:union,hands,DF.DataFrame(name="Gloves of the Messiah",int=17,mp5=10))
append!(cols=:union,hands,DF.DataFrame(name="Gloves of the Immortal",int=16))
append!(cols=:union,head,DF.DataFrame(name="Enigma Circlet",int=24,spi=12,sp=33,crit=0.01,hit=0.01,T2_5=1)) #
append!(cols=:union,head,DF.DataFrame(name="Don Rigoberto's Lost Hat",int=24,mp5=11))
append!(cols=:union,legs,DF.DataFrame(name="Enigma Leggings",int=26,spi=8,sp=34,crit=0.01,mp5=5,T2_5=1))
append!(cols=:union,neck,DF.DataFrame(name="Amulet of Vek'Nilash",int=5,sp=27,crit=0.01)) #
append!(cols=:union,neck,DF.DataFrame(name="Angelista's Charm",int=14,mp5=6))
append!(cols=:union,neck,DF.DataFrame(name="Necklace of Purity",int=13,sp=8))
append!(cols=:union,neck,DF.DataFrame(name="Amulet of the Fallen God",mp5=6))
append!(cols=:union,ranged,DF.DataFrame(name="Wand of Qiraji Nobilty",sp=19)) #
append!(cols=:union,shoulder,DF.DataFrame(name="Enigma Shoulderpads",int=12,spi=4,sp=30,mp5=5,T2_5=1))
append!(cols=:union,shoulder,DF.DataFrame(name="Ternary Mantle",int=17,spi=20))
append!(cols=:union,twohanded,DF.DataFrame(name="Blessed Qiraji Acolyte Staff",int=33,sp=76,hit=0.02,crit=0.01))
append!(cols=:union,twohanded,DF.DataFrame(name="Blessed Qiraji Augur Staff",int=24,mp5=15))
append!(cols=:union,twohanded,DF.DataFrame(name="Staff of the Qiraji Prophets",int=26,spi=8,sp=56,frostr=10,shadowr=10))
append!(cols=:union,onehanded,DF.DataFrame(name="Sharpened Silithid Femur",int=7,sp=72,crit=0.01))
append!(cols=:union,offhand,DF.DataFrame(name="Royal Scepter of Vek'lor",int=9,sp=20,crit=0.01,hit=0.01))
append!(cols=:union,offhand,DF.DataFrame(name="Sartura's Might",int=6,mp5=5))
append!(cols=:union,waist,DF.DataFrame(name="Eyestalk Waist Cord",int=9,crit=0.01,sp=41)) #
append!(cols=:union,wrist,DF.DataFrame(name="Bracelets of Royal Redemption",int=10,spi=9))
append!(cols=:union,wrist,DF.DataFrame(name="Burrower Bracers",int=13,sp=28))

#NAXXRAMAS
append!(cols=:union,chest,DF.DataFrame(name="Frostfire Robe",int=27,sp=47,hit=0.01,crit=0.01,T3=1)) #
append!(cols=:union,chest,DF.DataFrame(name="Crystal Webbed Robe",int=19,sp=53))
append!(cols=:union,chest,DF.DataFrame(name="Necro-Knight's Garb",sp=37)) #
append!(cols=:union,cloak,DF.DataFrame(name = "Cloak of the Necropolis",int = 12,sp = 26,crit=0.01,hit=0.01 )) #
append!(cols=:union,cloak,DF.DataFrame(name="Cloak of Suturing",int=12,spi=12,mp5=5))
append!(cols=:union,cloak,DF.DataFrame(name="Veil of Eclipse",int=10,sp=28))
append!(cols=:union,feet,DF.DataFrame(name="Frostfire Sandals",int=18,spi=10,crit=0.01,sp=28,T3=1)) #
append!(cols=:union,finger,DF.DataFrame(name="Frostfire Ring",sp=30,int=10, crit=0.01,T3=1)) #
append!(cols=:union,finger,DF.DataFrame(name="Ring of the Eternal Flame", int=10, firesp=34,crit=0.01)) #
append!(cols=:union,finger,DF.DataFrame(name="Ring of Spiritual Fervor", int=14, mp5=10)) #

append!(cols=:union,hands,DF.DataFrame(name="Frostfire Gloves",int=19,spi=10,sp=36,T3=1)) #
append!(cols=:union,head,DF.DataFrame(name="Frostfire Circlet",int=23,sp=35,hit=0.01,crit=0.02,T3=1)) #
append!(cols=:union,head,DF.DataFrame(name="Preceptor's Hat",int=24,sp=51))
append!(cols=:union,legs,DF.DataFrame(name="Frostfire Leggings",int=26,spi=10,hit=0.01,sp=48,T3=1)) #
append!(cols=:union,legs,DF.DataFrame(name="Leggings of Polarity",int=14,sp=44,crit=0.02))
append!(cols=:union,neck,DF.DataFrame(name="Pendant of Forgotten Names",int=18,spi=18,mp5=7)) #
append!(cols=:union,neck,DF.DataFrame(name="Gem of Trapped Innocents",int=7,sp=15,crit=0.02))
append!(cols=:union,neck,DF.DataFrame(name="Malice Stone Pendant",int=8,sp=28))
append!(cols=:union,neck,DF.DataFrame(name="Necklace of Necropsy",int=11,spi=10))
append!(cols=:union,ranged,DF.DataFrame(name ="Doomfinger",sp=16,crit=0.01))
append!(cols=:union,ranged,DF.DataFrame(name="Wand of the Whispering Dead",int=10,spi=9))
append!(cols=:union,ranged,DF.DataFrame(name="Wand of Fates",int=7,sp=12,hit=0.01)) #
append!(cols=:union,shoulder,DF.DataFrame(name="Frostfire Shoulderpads",int=18,spi=9,sp=36,T3=1)) #
append!(cols=:union,shoulder,DF.DataFrame(name="Rime Covered Mantle",int=12,sp=39,crit=0.01))
append!(cols=:union,trinket_active,DF.DataFrame(name="The Restrained Essence of Sapphiron",sp=40,cooldown=2*60,duration=20,type=:TREoS))
append!(cols=:union,trinket_active,DF.DataFrame(name="Warmth of Forgiveness",mp5=10,cooldown=3*60,type=:WoF))
append!(cols=:union,trinket,DF.DataFrame(name="Eye of Diminution",crit=0.02))
append!(cols=:union,twohanded,DF.DataFrame(name="Spire of Twilight",int=38,mp5=10))
append!(cols=:union,twohanded,DF.DataFrame(name="Soulseeker",int=31,sp=126,crit=0.02))
append!(cols=:union,onehanded,DF.DataFrame(name="Wraithblade",int=8,sp=95,crit=0.01,hit=0.01))
append!(cols=:union,onehanded,DF.DataFrame(name="Midnight Haze",int=12,sp=85))
append!(cols=:union,offhand,DF.DataFrame(name="Sapphiron Left Eye",int=8,sp=26,crit=0.01,hit=0.01))
append!(cols=:union,offhand,DF.DataFrame(name="Sapphiron Right Eye",int=10,mp5=4))
append!(cols=:union,offhand,DF.DataFrame(name="Noth's Frigid Heart",int=12,spi=13))
append!(cols=:union,offhand,DF.DataFrame(name="Digested Hand of Power",int=14,mp5=10))
append!(cols=:union,offhand,DF.DataFrame(name="Gem of Nerubis",int=10,sp=25))
append!(cols=:union,twohanded,DF.DataFrame(name="Brimstone Staff",int=30,hit=0.02,sp=113,crit=0.01)) #
append!(cols=:union,waist,DF.DataFrame(name="Frostfire Belt",int=21,spi=10,sp=28,hit=0.01,T3=1)) #
append!(cols=:union,wrist,DF.DataFrame(name="Frostfire Bindings",int=15,sp=27,T3=1)) #
append!(cols=:union,wrist,DF.DataFrame(name="The Soul Harvester's Bindings",int=11,crit=0.01,sp=21))

if undeadenemy
	append!(cols=:union,trinket,DF.DataFrame(name="Mark of the Champion",udsp=85)) #
	append!(cols=:union,trinket,DF.DataFrame(name="Rune of the Dawn",udsp=48))
end
if !frostimmune
	append!(cols=:union,chest,DF.DataFrame(name="Darkwater Robes",int=17,frostsp=39))
	append!(cols=:union,finger,DF.DataFrame(name="Freezing Band", frostsp=21,frostr=10))
	append!(cols=:union,ranged,DF.DataFrame(name="Cold Snap",int=7,frostsp=20))
	append!(cols=:union,ench_hands,DF.DataFrame(name="Enchant Gloves Frost Power",frostsp=20))
end
if !fireimmune
	append!(cols=:union,head,DF.DataFrame(name="Eye of Flame",int=10,spi=10,firesp=43))
	append!(cols=:union,legs,DF.DataFrame(name="Leggings of the Festering Swarm",int=23,firesp=57))
	append!(cols=:union,shoulder,DF.DataFrame(name="Mantle of Phrenic Power",int=20,firesp=33))
	append!(cols=:union,ench_hands,DF.DataFrame(name="Enchant Gloves Fire Power",firesp=20))
end
if rotation == :sapphiron
	append!(cols=:union,chest,DF.DataFrame(name="Glacial Vest",sp=21,frostr=40)) #
	append!(cols=:union,cloak,DF.DataFrame(name="Frostweaver Cape",int=12,spi=12,frostr=10))
	append!(cols=:union,cloak,DF.DataFrame(name="Glacial Cloak",frostr=24)) #
	append!(cols=:union,cloak,DF.DataFrame(name="Sapphiron Drape",int=17,sp=14,frostr=6)) #
	append!(cols=:union,hands,DF.DataFrame(name="Glacial Gloves",sp=15,frostr=30)) #
##	append!(cols=:union,head,DF.DataFrame(name="Dragonskin Cowl of Frost Resistance",int=15,frostr=15,sp=18))
	append!(cols=:union,head,DF.DataFrame(name="Glacial Headdress",int=21,sp=18,frostr=40)) #
	append!(cols=:union,finger,DF.DataFrame(name="Band of Icy Depths",frostr=20))
##	append!(cols=:union,finger,DF.DataFrame(name="Elemental Circle", frostr=10))
	append!(cols=:union,finger,DF.DataFrame(name="Hailstone Band",frostr=20))
	append!(cols=:union,finger,DF.DataFrame(name="Ramaladni's Icy Grasp",frostr=25)) #
##	append!(cols=:union,finger,DF.DataFrame(name="Seal of Ascension", frostr=10)) #
	append!(cols=:union,legs,DF.DataFrame(name="Glacial Leggings",sp=18,frostr=40)) #
	append!(cols=:union,legs,DF.DataFrame(name="Leggings of Arcane Supremacy",int=24,spi=14,frostr=10))
	append!(cols=:union,neck,DF.DataFrame(name="Tempestria's Frozen Necklace",int=5,frostr=15))
	append!(cols=:union,neck,DF.DataFrame(name="Touch of Frost",frostr=24)) #
	append!(cols=:union,ranged,DF.DataFrame(name="Banshee Finger",frostr=10)) #
	append!(cols=:union,shoulder,DF.DataFrame(name="Glacial Mantle",sp=16,frostr=33))
	append!(cols=:union,trinket,DF.DataFrame(name="Frostwolf Insignia Rank 6",frostr=10)) #
	append!(cols=:union,trinket,DF.DataFrame(name="Gyrofreeze Ice Reflector",frostr=15))
	append!(cols=:union,waist,DF.DataFrame(name="Frostwolf Cloth Belt",int=10,frostr=5,sp=18))
	append!(cols=:union,wrist,DF.DataFrame(name="Glacial Wrists",sp=12,frostr=20)) #
	append!(cols=:union,ench_head,DF.DataFrame(name="Ice Guard",frostr=10))
	append!(cols=:union,ench_legs,DF.DataFrame(name="Ice Guard",frostr=10))
	append!(cols=:union,ench_shoulder,DF.DataFrame(name="Frost Mantle of the Dawn",frostr=5))	
end
if rotation == :twins
	append!(cols=:union,cloak,DF.DataFrame(name="Chromatic Cloak",crit=0.01,shadowr=9))
	append!(cols=:union,cloak,DF.DataFrame(name="Cloak of Untold Secrets",shadowr=20))
	append!(cols=:union,cloak,DF.DataFrame(name="Juno's Shadow",int=5,shadowr=15))
	append!(cols=:union,chest,DF.DataFrame(name="Necrology Robes",int=12,shadowr=5))
	append!(cols=:union,feet,DF.DataFrame(name="Runed Stygian Boots",shadowr=20,mp5=4))
	append!(cols=:union,finger,DF.DataFrame(name="Ukko's Ring of Darkness", shadowr=20))
	append!(cols=:union,hands,DF.DataFrame(name = "Darkbind Gloves",shadowr=20)) #
	append!(cols=:union,hands,DF.DataFrame(name = "Shadowy Laced Handwraps",shadowr=12,int=15))
	append!(cols=:union,legs,DF.DataFrame(name="Runed Stygian Leggings",mp5=6,shadowr=25))
	append!(cols=:union,neck,DF.DataFrame(name="Amulet of Shadow Shielding",spi=7,shadowr=20)) #
	append!(cols=:union,ranged,DF.DataFrame(name="Gravestone Scepter",shadowr=5,spi=1))
	append!(cols=:union,ranged,DF.DataFrame(name="Serpentine Skuller",shadowr=10))
	append!(cols=:union,shoulder,DF.DataFrame(name="Argent Shoulders",spi=8,shadowr=5))
	append!(cols=:union,trinket,DF.DataFrame(name="Ultra-Flash Shadow Reflector",shadowr=20))
	append!(cols=:union,waist,DF.DataFrame(name="Runed Stygian Belt",shadowr=20,mp5=3))
	append!(cols=:union,wrist,DF.DataFrame(name="Funeral Cuffs",int=14,spi=5,shadowr=10))
	append!(cols=:union,ench_head,DF.DataFrame(name="Shadow Guard",shadowr=10))
	append!(cols=:union,ench_legs,DF.DataFrame(name="Shadow Guard",shadowr=10))
	append!(cols=:union,ench_shoulder,DF.DataFrame(name="Shadow Mantle of the Dawn",shadowr=5))
end
if rotation == :sapphiron || rotation == :twins
	append!(cols=:union,chest,DF.DataFrame(name="Elemental Raiment",frostr=5,shadowr=5,sp=21))
	append!(cols=:union,finger,DF.DataFrame(name="Ring of Binding",frostr=10,shadowr=10)) #
	append!(cols=:union,trinket_active,DF.DataFrame(name="Loatheb's Reflection",frostr=13,shadowr=13)) #
	append!(cols=:union,trinket,DF.DataFrame(name="Smoking Heart of the Mountain",frostr=7,shadowr=7))
end

if rotation == :bwldragons
	cloak::DF.DataFrame = DF.DataFrame()
	append!(cols=:union,cloak,DF.DataFrame(name="Onyxia Scale Cloak")) #
end

append!(cols=:union,trinket_both,trinket)
append!(cols=:union,trinket_both,trinket_active)
append!(cols=:union,weapon,onehanded)
append!(cols=:union,weapon,twohanded)

slots[ifinger] = coalesce.(finger,0)
slots[itrinket] = coalesce.(trinket_both,0)
slots[ichest] = coalesce.(chest,0)
slots[icloak] = coalesce.(cloak,0)
slots[ifeet] = coalesce.(feet,0)
slots[ihands] = coalesce.(hands,0)
slots[ihead] = coalesce.(head,0)
slots[ilegs] = coalesce.(legs,0)
slots[ineck] = coalesce.(neck,0)
slots[ioffhand] = coalesce.(offhand,0)
slots[iranged] = coalesce.(ranged,0)
slots[ishoulder] = coalesce.(shoulder,0)
slots[iweapon] = coalesce.(weapon,0)
slots[iwaist] = coalesce.(waist,0)
slots[iwrist] = coalesce.(wrist,0)

append!(cols=:union,ench_chest,DF.DataFrame(name="Enchant Chest Greater Stats",spi=4,int=4))
append!(cols=:union,ench_chest,DF.DataFrame(name="Enchant Chest Major Mana",mana=100))
slots[iench_chest] = coalesce.(ench_chest,0)

slots[iench_hands] = coalesce.(ench_hands,0)

append!(cols=:union,ench_head,DF.DataFrame(name="Lesser Arcanum of Rumination",mana=150))
append!(cols=:union,ench_head,DF.DataFrame(name="Lesser Arcanum of Voracity Spirit",spi=8))
append!(cols=:union,ench_head,DF.DataFrame(name="Lesser Arcanum of Voracity Intellect",int=8))
append!(cols=:union,ench_head,DF.DataFrame(name="Presence of Sight",sp=18,hit=0.01))
##append!(cols=:union,ench_head,DF.DataFrame(name="Arcanum of Focus",sp=8))
slots[iench_head] = coalesce.(ench_head,0)

#ench_feet = DF.DataFrame(name="Enchant Boots Spirit",spi=5)

append!(cols=:union,ench_legs,DF.DataFrame(name="Lesser Arcanum of Rumination",mana=150))
append!(cols=:union,ench_legs,DF.DataFrame(name="Lesser Arcanum of Voracity Spirit",spi=8))
append!(cols=:union,ench_legs,DF.DataFrame(name="Lesser Arcanum of Voracity Intellect",int=8))
append!(cols=:union,ench_legs,DF.DataFrame(name="Presence of Sight",sp=18,hit=0.01))
##append!(cols=:union,ench_legs,DF.DataFrame(name="Arcanum of Focus",sp=8))
slots[iench_legs] = coalesce.(ench_legs,0)

append!(cols=:union,ench_shoulder,DF.DataFrame(name="Zandalar Signet of Mojo",sp=18))
append!(cols=:union,ench_shoulder,DF.DataFrame(name="Resilience of the Scourge",mp5=5))
append!(cols=:union,ench_shoulder,DF.DataFrame(name="Power of the Scourge",sp=15,crit=0.01))
slots[iench_shoulder] = coalesce.(ench_shoulder,0)

append!(cols=:union,ench_weapon,DF.DataFrame(name="Enchant Weapon Migthy Spirit",spi=20))
append!(cols=:union,ench_weapon,DF.DataFrame(name="Enchant Weapon Mighty Intellect",int=22))
append!(cols=:union,ench_weapon,DF.DataFrame(name="Enchant Weapon Spell Power",sp=30))
slots[iench_weapon] = coalesce.(ench_weapon,0)

append!(cols=:union,ench_wrist,DF.DataFrame(name="Enchant Bracer Mana Regeneration",mp5=4))
append!(cols=:union,ench_wrist,DF.DataFrame(name="Enchant Bracer Greater Intellect",int=7))
append!(cols=:union,ench_wrist,DF.DataFrame(name="Enchant Bracer Superior Spirit",spi=9))
slots[iench_wrist] = coalesce.(ench_wrist,0)
