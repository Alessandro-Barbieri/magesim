#     Copyright (C) 2013  Alessandro Barbieri
# 
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU Affero General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
# 
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU Affero General Public License for more details.
# 
#     You should have received a copy of the GNU Affero General Public License
#     along with this program.  If Not, see <https://www.gnu.org/licenses/>.

using Revise
import NSGAII
import UnicodePlots
import Serialization
using Base.Threads
#import FinitePosets
import Statistics
import StatsBase
import InvertedIndices
#using Evolutionary
import DataFrames as DF
import Random
#using Memoization
includet("constants.jl")
includet("mage.jl")
import .MageModule: Mage, Target, Simulation, state_machine!, level_to_hit
import Base.show

mutable struct NoisyFitness <: Real
	x::Vector
	m::Float64
	y::Vector
end

Base.show(io::IO, nf::NoisyFitness) = show(io, nf.m)
Base.zero(x::NoisyFitness) = NoisyFitness([],0.0,[])
Base.zero(::Type{NoisyFitness}) = NoisyFitness([],0.0,[])
function Base.:<(a::NoisyFitness, b::NoisyFitness)
	print("pippo")
	return signtest(a,b)
end
Base.convert(::Type{T}, nf::NoisyFitness) where {T<:Real} = nf.m
Base.convert(::Type{NoisyFitness},v::T) where {T<:AbstractVector} = NoisyFitness(v)
(::Type{T})(nf::NoisyFitness) where T<:AbstractFloat = Base.convert(T, nf)
Base.typemax(::Type{NoisyFitness})=NoisyFitness([],Typemax(Float64),[])

minimal=zeros(NoisyFitness,1)
const stato=Dict{Tuple,Vector{NoisyFitness}}()
fitnessl=3

#ENV["JULIA_DEBUG"] = "MageModule"

@enum Slots_idx ifinger=1 itrinket=2 ichest=3 icloak=4 ifeet=5 ihands=6 ihead=7 ilegs=8 ineck=9 ioffhand=10 iranged=11 ishoulder=12 iwaist=13 iweapon=14 iwrist=15 iench_chest=16 iench_hands=17 iench_head=18 iench_legs=19 iench_shoulder=20 iench_weapon=21 iench_wrist=22
Base.to_index(s::Slots_idx)=Int(s)
slots::Vector{DF.DataFrame} = Vector{DF.DataFrame}(undef,22)

include("items.jl")

nonehanded=DF.nrow(onehanded)

n::Vector = [DF.nrow(s) for s in slots]
pushfirst!(n,DF.nrow(slots[ifinger]),DF.nrow(slots[itrinket]))

function decode1(x::BitVector)
	local indexes::Vector = [min(1+evalpoly(2,x[a+1:b]),c) for (a,b,c) in zip(vcat(0,s),s,n)]
	return indexes
end

function encode1(idx::Vector)
	local y = Vector(undef,0)
	for (i,c,p) in zip(idx,n,l)
		append!(y,digits(min(i,c)-1,base=2,pad=p))
	end
	local x = BitVector(y)
	return x
end

function mage_from_stats(statistics::DF.DataFrame)
	local m = Mage()

	for i in [:WoC,:EC,:DCBD,:RotA,:RoI,:BoED]
		if i in propertynames(statistics)
			push!(m.s.special,i)
		end
	end
	if :trinket1 in propertynames(statistics)
		m.s.trinket1=statistics[1,:trinket1]
	end
	if :trinket2 in propertynames(statistics)
		m.s.trinket2=statistics[1,:trinket2]
	end
	m.s.frostr = +(statistics[1,:frostr],mage_armor_resistance)
	m.s.shadowr = +(
		shadow_resistance,
		statistics[1,:shadowr],
		mage_armor_resistance
	)
	m.s.T2 = statistics[1,:T2]
	m.s.T2_5 = statistics[1,:T2_5]
	m.s.T3 = statistics[1,:T3]
	m.s.mp5 = statistics[1,:mp5]
	m.s.int = +(
		statistics[1,:int],
		base_int,
		arcane_intellect
	)
	m.s.spi = statistics[1,:spi] + base_spi
	m.s.crit = statistics[1,:crit]
	m.s.firesp = statistics[1,:firesp]
	m.s.frostsp = statistics[1,:frostsp]
	if undeadenemy
		m.s.sp  = statistics[1,:sp] + statistics[1,:udsp]
	else
		m.s.sp  = statistics[1,:sp]
	end

	if consumables
		m.s.shadowr += gift_of_arthas
		m.s.frostr += juju_chill
		m.s.mp5 += mageblood_potion
		m.s.int += +(kreegs_stout_beatdown_int,runn_tum_tuber_surprise)
		m.s.spi += kreegs_stout_beatdown_spi
		m.s.crit += brilliant_wizard_oil_crit
		m.s.sp += +(
			brilliant_wizard_oil_sp,
			greater_arcane_elixir,
			flask_of_supreme_power
		)
		m.s.firesp += elixir_of_greater_firepower
		m.s.frostsp += elixir_of_frost_power
	end
	if raid_buffs
		m.s.frostr += max(frost_resistance_totem,mark_of_the_wild_resistances)
		m.s.shadowr += max(shadow_protection,mark_of_the_wild_resistances)
		m.s.mp5 += +(power_of_the_guardian_druid,mana_spring_totem_mp5)
		m.s.int += mark_of_the_wild
		m.s.spi += +(mark_of_the_wild,prayer_of_spirit)
		m.s.crit += +(power_of_the_guardian_mage,moonkin_aura)
		m.s.sp += power_of_the_guardian_warlock
	end
	if world_buffs
		m.s.mp5 += warchiefs_blessing
		m.s.int += songflower_serenade
		m.s.spi += +(spirit_of_zanza,songflower_serenade)
		m.s.crit += +(
			slipkiks_savvy,
			songflower_serenade_crit,
			rallying_cry_of_the_dragonslayer
		)
		m.s.int *= spirit_of_zandalar
		m.s.spi *= spirit_of_zandalar
	end
	m.s.crit += 0.01*m.s.int/54.0

	m.s.max_mana = +(
		20.0,
		15.0*(m.s.int - 20.0),
		base_mana,
		statistics[1,:mana]
	)
	m.s.spirit_reg = (12.5 + m.s.spi/4.0)/2.0
	m.current_mana = m.s.max_mana
	m.s.hit = +(elemental_precision_2,statistics[1,:hit])
	return m
end

function fitness_generator(n,y::Vector)
	local m = mage_from_stats(stats_from_items(y))

	local players = Vector{Mage}(undef,nplayers)
	for i in 1:nplayers
		players[i] = deepcopy(m)
		players[i].id=i
	end
	local tgt=Target()
	tgt.debuffs.fireball_dot=zeros(Bool,nplayers)
	local sim=Simulation()
	local x1::Vector{Float64} = Vector(undef,n)
	local x2::Vector{Float64} = Vector(undef,n)
	@threads for i in 1:n
		x1[i],x2[i]=state_machine!(deepcopy(sim),deepcopy(players),deepcopy(tgt))
#		x1[i],_=state_machine!(deepcopy(sim),deepcopy(players),deepcopy(tgt))
	end
	return x1,x2
#	return x1
end

function vector2fitness(v,y::Vector)
	return NoisyFitness(sort(v),Statistics.median(v),y)
end

function vector2fitness!(f::NoisyFitness,v)
	f.x = sort(v)
	f.m=Statistics.median(v)
end

function fitness_jl(y::Vector)
	local k=tuple(y...)
	local n::Int=1000
	local fg1::Vector{Float64}=zeros(Float64,n)
	local fg2::Vector{Float64}=zeros(Float64,n)
	if !haskey(stato,k)
		fg1,fg2=fitness_generator(n,y)
#		fg1=fitness_generator(n,y)
		stato[k]=zeros(NoisyFitness,2)
		stato[k][1]= vector2fitness(fg1./nplayers,y)
		stato[k][2]=vector2fitness(fg2,y)
	else
#		fg1,fg2=fitness_generator(n,y)
#		fg1=	fitness_generator(n,y)
#		vector2fitness!(stato[k][1],-fg1./nplayers)
#		vector2fitness!(stato[k][2],-fg2)
	end
	return stato[k][1].m,min(315,c(y)[1])
#	return [stato[k][1].m + r*10.0 + t*100,stato[k][2].m + r*0.1 + t * 10]
#	return [-stato[k][1].l/nplayers,-stato[k][1].u/nplayers]
#	mit = (0.75*m.s.frostr/315.0 -3.0/16.0*max(0,m.s.frostr/315.0))
#	return [-E,-min(m.s.frostr,315.0)]
end

function stats_from_items(idx::Vector)
	local ite = DF.DataFrame()
	for i in 3:length(slots)
		if i != ioffhand || idx[iweapon] <= nonehanded
			push!(cols=:union,ite,slots[i-2][idx[i],:])
		end
	end
	if idx[1] < idx[3]
		push!(cols=:union,ite,slots[ifinger][idx[1],:])
	end
	if idx[2] < idx[4]
		push!(cols=:union,ite,slots[itrinket][idx[2],:])
	end
	local total = DF.combine(coalesce.(ite,0),names(ite,InvertedIndices.Not([:name,:type])) .=> sum,renamecols=false)
	if :type in propertynames(slots[itrinket][idx[2],:type])
		insertcols!(total,:trinket1 => slots[itrinket][idx[2],:type])
	end
	if :type in propertynames(slots[itrinket][idx[4],:type])
		insertcols!(total,:trinket2 => slots[itrinket][idx[4],:type])
	end
	if total[1,:postmaster] >= 4
		total[1,:sp] += 12
	end
	if total[1,:postmaster] >= 5
		total[1,:int] += 10
	end
	if total[1,:ZG] >= 2
		total[1,:sp] += 12.0
	end
	if total[1,:T0] >= 4
		total[1,:sp] += 23.0
	end
	if total[1,:T0_5] >= 6
		total[1,:sp] += 23.0
	end
	if total[1,:T1] >= 3
		total[1,:sp] += 18.0
	end
	if total[1,:T3] >= 4
		total[1,:frostr] += 35.0
		total[1,:shadowr] += 35.0
	end
	return total
end

function names1(idx)
	local nam = Dict(Symbol(Slots_idx(i)) => slots[i][idx[i+2],:name] for i in 3:length(slots))
	nam[:finger1] = slots[ifinger][idx[1],:name]
	nam[:finger2] = slots[ifinger][idx[3],:name]
	nam[:trinket1] = slots[itrinket][idx[2],:name]
	nam[:trinket2] = slots[itrinket][idx[4],:name]
	return nam
end

function mymutation(recombinant::T; rng::Random.AbstractRNG=Random.default_rng()) where {T <: AbstractVector}
	local idx::T = recombinant
	local i = StatsBase.sample(rng,StatsBase.Weights(n))
	idx[i] = rand(rng,1:n[i])
	recombinant = idx
	return recombinant
end

function mycrossover(v1::T,v2::T; rng::Random.AbstractRNG=Random.default_rng()) where {T <: AbstractVector}
	local idx1::T = copy(v1)
	local idx2::T = copy(v2)
	local i=1
	for (a,b) in zip(idx1,idx2)
		if rand(rng,Bool)
			idx1[i]=b
			idx2[i]=a
		end
		i=i+1
	end
	return idx1, idx2
end

function initpop()
	local x= [rand(1:i) for i in n]
	return x
end

#x1=0x9e32a67eff81c695
#x2=0x49a

#lv 60, 30min
#x1=0xf67a737ed76fd4f0
#x2=0x17c

#naxx
#x1 = 0x0f3272e240ede64b
#x2 = 0x59f

#frostbolt
#x1 = 0x0d56a7b2ea2d79c2
#x2 = 0x765

#p = s[end] - ndigits(x1,base=2)
#initpop = BitVector(append!(digits(x1,base=2),digits(x2,base=2,pad=p)))

function fast_nondominated_sort(P::DF.DataFrame)
	local F = Vector()
	push!(F,DF.DataFrame())
	for p in eachrow(P)
		local Sp = DF.DataFrame() 
		local np=0
		for q in eachrow(P)
			local o = DF.combine(coalesce.(vcat(p,q,cols=:union),0),names(p,InvertedIndices.Not([:name])) .=> <,renamecols=false)
	

			if all(p[InvertedIndices.Not(:name)] .< q[InvertedIndices.Not(:name)])
				push!(Sp,q)
			elseif all(q[InvertedIndices.Not(:name)] .< p[InvertedIndices.Not(:name)])
				np = np + 1
			end
			if np == 0
				prank = 1
				push!(F[i],p)
			end
		end
	end
	local i = 1
	while nrow(F[i]) > 0
		local Q = DF.DataFrame()
		for p in eachrow(F[i])
			for q in eachrow(Sp)
				nq = nq -1
				if nq <= 0
					qrank = i + 1
					push!(Q,q)
				end
			end
		end
		i = i + 1
		push!(F,Q)
	end
end

function c(y)
	local m = mage_from_stats(stats_from_items(y))
	return [m.s.frostr,m.s.T3]
end

#=
function Evolutionary.trace!(record::Dict{String,Any}, objfun, state, population, method::Evolutionary.NSGA2, options)
	if length(state.fitness[1,:]) > 0
		record["fittest"]=state.fittest
		local n=length(population)
		local x=state.fitpop[1,:]
		local y=state.fitpop[2,:]
		if ! ("plot" in keys(record))
			record["plot"] = []
		end
		push!(record["plot"],UnicodePlots.scatterplot(x,y))
	end
end
function Evolutionary.trace!(record::Dict{String,Any}, objfun, state, population, method::Evolutionary.GA, options)
	if length(population) > 0
		local P=FinitePosets.Poset((x,y) -> x.u<y.l,[stato[i...] for i in unique(population)])
		global minimal = FinitePosets.minima(P)
	    record["best"] =sort(minimal,by=(x)->x.u,rev=true)
	end
end
=#


function mymut!(v)
	v = mymutation(v)
end

function xover!(p1,p2,c1,c2)
	c1,c2 = mycrossover(p1,p2)
end

function plot_pop(P)
	P = filter(indiv -> indiv.rank==1,P)
	for p in P
		display(names1(p.x))
		println(p.y)
	end
	local plt = UnicodePlots.scatterplot(map(x -> x.y[1], P), map(x -> x.y[2],P))
	display(plt)
end

#function Evolutionary.penalty!(fitness, constraints, population)
#end

#=
function Evolutionary.evaluate!(objfun, fitness, population, constraints)
    # calculate fitness of the population
    Evolutionary.value!(objfun, fitness, population)
    # apply penalty to fitness
    Evolutionary.penalty!(fitness, constraints, population)

#	sp=sortperm(fitness,dims=1)
#	bestidx=sp[1]
#	t=tuple(population[bestidx]...)
#	for i in 1:size(fitness,2)
#		fitness[2,i]=fitness[1,i] -stato[t].u
#	end
	global fitnessl=fitnessl+1
end
=#

function stochastic_tournament(;select=argmin)
    function tournamentN(fitness::AbstractVecOrMat{<:Real}, N::Int;
                         rng::Random.AbstractRNG=default_rng())
        groupSize::Int = rand(1:N)
        selection = fill(0,N)
        sFitness = size(fitness)
        d, nFitness = length(sFitness) == 1 ? (1, sFitness[1]) : sFitness
        tour = Random.randperm(rng, nFitness)
        j = 1
        for i in 1:N
            idxs = tour[j:j+groupSize-1]
            selected = d == 1 ? view(fitness, idxs) : view(fitness, :, idxs)
            winner = select(selected)
            selection[i] = idxs[winner]
            j+=groupSize
            if (j+groupSize) >= nFitness && i < N
                tour = Random.randperm(rng, nFitness)
                j = 1
            end
        end
        return selection
    end
    return tournamentN
end

function mycallback(trace)
#	for i in trace[end].metadata["best"]
#		display(i)
#		display(names1([i.y...]))
#		display(i.x0)
#	end
#	display(length(minimal))

#	display(fieldnames(typeof(trace[end])))
	sp=sortperm(trace[end].value[1,:])
	for i in sp
		display(names1(trace[end].metadata["fittest"][i]))
		display(c(trace[end].metadata["fittest"][i]))
		display(stato[trace[end].metadata["fittest"][i]...])
		display(trace[end].value[:,i])
	end
	if "plot" in keys(trace[end].metadata)
		for i in length(trace[end].metadata["plot"])
			display(trace[end].metadata["plot"][i])
		end
	end
#	display(trace[end].metadata["plot"])
	return false
end

xinit=[initpop() for i in 1:3]
xinit[1] = [
	findfirst((finger.name .== "Ring of the Fallen God")),
	findfirst((trinket_both.name .== "Mark of the Champion")),
	findfirst((finger.name .== "Ring of the Eternal Flame")),
	findfirst((trinket_both.name .== "Neltharion's Tear")),
	findfirst((chest.name .== "Frostfire Robe")),
	findfirst((cloak.name .== "Cloak of the Devoured")),
	findfirst((feet.name .== "Frostfire Sandals")),
	findfirst((hands.name .== "Dark Storm Gauntlets")),
	findfirst((head.name .== "Frostfire Circlet")),
	findfirst((legs.name .== "Frostfire Leggings")),
	findfirst((neck.name .== "Amulet of Vek'Nilash")),
	1,
	findfirst((ranged.name .== "Wand of Qiraji Nobilty")),
	findfirst((shoulder.name .== "Frostfire Shoulderpads")),
	findfirst((waist.name .== "Eyestalk Waist Cord")),
	findfirst((weapon.name .== "Brimstone Staff")),
	findfirst((wrist.name .== "Frostfire Bindings")),
	findfirst((ench_chest.name .== "Enchant Chest Greater Stats")),
	findfirst((ench_hands.name .== "Enchant Gloves Fire Power")),
	findfirst((ench_head.name .== "Presence of Sight")),
	findfirst((ench_legs.name .== "Presence of Sight")),
	findfirst((ench_shoulder.name .== "Power of the Scourge")),
	findfirst((ench_weapon.name .== "Enchant Weapon Spell Power")),
	findfirst((ench_wrist.name .== "Enchant Bracer Greater Intellect"))
]
xinit[2] = [
	findfirst((finger.name .== "Frostfire Ring")),
	findfirst((trinket_both.name .== "Gyrofreeze Ice Reflector")),
	findfirst((finger.name .== "Ramaladni's Icy Grasp")),
	findfirst((trinket_both.name .== "Loatheb's Reflection")),
	findfirst((chest.name .== "Glacial Vest")),
	findfirst((cloak.name .== "Sapphiron Drape")),
	findfirst((feet.name .== "Frostfire Sandals")),
	findfirst((hands.name .== "Glacial Gloves")),
	findfirst((head.name .== "Glacial Headdress")),
	findfirst((legs.name .== "Glacial Leggings")),
	findfirst((neck.name .== "Touch of Frost")),
	1,
	findfirst((ranged.name .== "Banshee Finger")),
	findfirst((shoulder.name .== "Frostfire Shoulderpads")),
	findfirst((waist.name .== "Frostwolf Cloth Belt")),
	findfirst((weapon.name .== "Elemental Mage Staff")),
	findfirst((wrist.name .== "Glacial Wrists")),
	findfirst((ench_chest.name .== "Enchant Chest Greater Stats")),
	findfirst((ench_hands.name .== "Enchant Gloves Fire Power")),
	findfirst((ench_head.name .== "Ice Guard")),
	findfirst((ench_legs.name .== "Ice Guard")),
	findfirst((ench_shoulder.name .== "Frost Mantle of the Dawn")),
	findfirst((ench_weapon.name .== "Enchant Weapon Spell Power")),
	findfirst((ench_wrist.name .== "Enchant Bracer Greater Intellect"))
]
xinit[3] = [
	findfirst((finger.name .== "Ring of the Fallen God")),
	findfirst((trinket_both.name .== "Neltharion's Tear")),
	findfirst((finger.name .== "Ring of the Eternal Flame")),
	findfirst((trinket_both.name .== "Mark of the Champion")),
	findfirst((chest.name .== "Frostfire Robe")),
	findfirst((cloak.name .== "Sapphiron Drape")),
	findfirst((feet.name .== "Frostfire Sandals")),
	findfirst((hands.name .== "Glacial Gloves")),
	findfirst((head.name .== "Glacial Headdress")),
	findfirst((legs.name .== "Glacial Leggings")),
	findfirst((neck.name .== "Amulet of Vek'Nilash")),
	1,
	findfirst((ranged.name .== "Wand of Qiraji Nobilty")),
	findfirst((shoulder.name .== "Frostfire Shoulderpads")),
	findfirst((waist.name .== "Frostfire Belt")),
	findfirst((weapon.name .== "Elemental Mage Staff")),
	findfirst((wrist.name .== "Glacial Wrists")),
	findfirst((ench_chest.name .== "Enchant Chest Greater Stats")),
	findfirst((ench_hands.name .== "Enchant Gloves Fire Power")),
	findfirst((ench_head.name .== "Ice Guard")),
	findfirst((ench_legs.name .== "Ice Guard")),
	findfirst((ench_shoulder.name .== "Frost Mantle of the Dawn")),
	findfirst((ench_weapon.name .== "Enchant Weapon Spell Power")),
	findfirst((ench_wrist.name .== "Enchant Bracer Greater Intellect"))
]


function CV(x)
	if all(x .>= 1) && all( x .<= n)
		return 0
	else
		return 1
	end
end

function main()
#	gr()
#	unicodeplots()
	 #fast_nondominated_sort(finger)


#	if rotation == :sapphiron
#		local con = Evolutionary.PenaltyConstraints(1000.0,0.0 .* n,1.0 .* n,[315.0],[Inf],c)
#	end
#	local result = Evolutionary.optimize(fitness_jl,con,
#		initpop,
#		NSGA2(
#			populationSize=100,
#			selection = Evolutionary.tournament(4,select=Evolutionary.twowaycomp),
#			selection = stochastic_tournament(select=Evolutionary.twowaycomp),
#			crossover = mycrossover,
#			mutation = mymutation,
#			ɛ=0.5,
#			crossoverRate=0.85,
#			mutationRate = 0.15
#		),
#		Evolutionary.Options(
#			show_trace=true,
#			store_trace=true,
#			iterations=10000,
#			successive_f_tol=100,
#			callback=mycallback
#		)
#	)

	local result = NSGAII.nsga_max(1000,100,fitness_jl,initpop,fCV=CV,fplot=plot_pop,fcross=xover!,fmut=mymut!,showprogress=false,seed=xinit)

	return result

#=
y=Serialization.deserialize("63.dat")
xx=fill([],DF.nrow(trinket_both))
for j in 1:DF.nrow(trinket_both)
	if j < 7
		y.minimizer[4]=7
		y.minimizer[2]=j
	else
		y.minimizer[2]=7
		y.minimizer[4]=j
	end
	local m::Mage = mage_from_stats(stats_from_items(y.minimizer))

	local players::Vector{Mage} = Vector{Mage}(undef,nplayers)
	for i in 1:nplayers
		players[i] = deepcopy(m)
		players[i].id=i
	end
	local tgt::Target=Target()
	tgt.debuffs.fireball_dot=zeros(Bool,nplayers)
	local sim::Simulation=Simulation()
	xx[j]=fitness_generator(10000,sim,players,tgt)
end
	mx=Statistics.mean.(xx)
	return sortperm(mx), mx
=#
end

# function hamming(a::BitVector,b::BitVector)
# 	return count(xor.(a,b))
# end
# 
# function diversity(b::BitVector,pop,t)
# 	local H = mean(hamming.(pop[i],b))
# 	return exp(-H/t)
# end
# 
# function quality(a,b)
# 	return a/(a*a + b*b)
# end
# 
# function performance(p,t)
# 	local C1 = diversity(fmin,p,t)
# 	local C2 = quality(fmin,fmax)
# 	return (C1 + (t-1)*C2)/t
# end
# 
function signtest(a,b)
	local tau = 0.5
	return sum(sign.(a .- b))/length(a) > tau
end

function myselection(fitness, N; rng::Random.AbstractRNG=default_rng())
    local selection = fill(0,N)
	local l=length(fitness)
	for i in 1:N
		local a = rand(rng,1:l)
		local b = rand(rng,1:l)
		if signtest(fitness[a],fitness[b])
			selection[i]=a
		else			
			selection[i]=b
		end
	end
	return selection
end

##
## Gray coding for binary genetic algorithm Based on algorithm on Eiben Smith (2003) Introduction to 
## Evolutionary Computing
##
# function binary2gray(x::BitVector)
# 	local n = length(x)
# 	local g::BitVector =BitVector(undef,n)
# 	g[1] = x[1]
# 	if n > 1
# 		for i in 2:n
# 			g[i] = xor(x[i-1], x[i])
# 		end
# 	end
# 	return g
# end
# function gray2binary(x::BitVector) 
# 	local n = length(x)
# 	local b::BitVector = BitVector(undef, n)
# 	local value = x[1]
# 	b[1] = value
# 	if n > 1
# 		for i in 2:n
# 			if x[i]
# 				value = ~value
# 			end
# 			b[i] = value
# 		end
# 	end
# 	return b
# end 

r=main()
#Serialization.serialize("paretoSapphiron.dat",r)
#j=sortperm([i.value for i in r.trace])
#display(names1(r.trace[j[1]].metadata["best"]))
