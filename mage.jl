#     Copyright (C) 2013  Alessandro Barbieri
# 
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU Affero General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
# 
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU Affero General Public License for more details.
# 
#     You should have received a copy of the GNU Affero General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.

module MageModule
	import StatsBase

	include("constants.jl")

	mutable struct Event
		time::Real
		owner::Integer
		data::Dict
	end
	@kwdef mutable struct Stat
		special::Vector{Symbol} = Vector{Symbol}()
		trinket1::Symbol = Symbol()
		trinket2::Symbol = Symbol()
		T2::Integer =0
		T2_5::Integer = 0
		T3::Integer=0
		crit::Real =0.0
		firesp::Real =0.0
		frostsp::Real =0.0
		frostr::Real=0.0
		shadowr::Real=0.0
		sp::Real =0.0
		hit::Real =0.0
		int::Real =0.0
		max_mana::Real =0.0
		mp5::Real =0.0
		spi::Real = 0.0
		spirit_reg::Real =0.0
	end
	@kwdef mutable struct Cd
		burst_of_knowledge::Bool = false
		clearcast::Bool = false
		combustion::Bool = false
		darkmoon_card_blue_dragon::Bool = false
		energy_cloak::Bool = false
		evocation::Bool = false
		eye_of_moam::Bool = false
		fire_ruby::Bool = false
		gcd::Bool = false
		marlis_eye::Bool = false
		mind_quickening_gem::Bool = false
		minor_potion::Bool = false
		potion::Bool = false
		robe_of_the_archmage::Bool = false
		robes_of_insight::Bool = false
		shared_trinket::Bool = false
		talisman_of_ascendance::Bool = false
		talisman_of_ephemeral_power::Bool = false
		the_restrained_essence_of_sapphiron::Bool = false
		warmth_of_forgiveness::Bool = false
		zandalarian_hero_charm::Bool = false
	end
	@kwdef mutable struct Debuffs
		ignite_damage::Real = 0.0
		ignite_stacks::Integer = 0
		ignite_stacks_type::Vector{Union{Symbol,Nothing}} = fill(nothing,5)
		elemental_vulnerability::Bool = false
		fireball_dot::Vector{Bool}=zeros(Bool,1)
		scorch_stacks::Integer = 0
	end
	@kwdef mutable struct Buffs
		drinking::Bool = false
		ascendance::Bool = false
		aura_of_the_blue_dragon::Bool = false
		burst_of_knowledge::Bool = false
		chaos_fire::Bool = false
		chromatic_infusion::Bool = false
		clearcast::Bool = false
		combustion::Bool = false
		ephemeral_power::Bool = false
		essence_of_sapphiron::Bool = false
		evocation::Bool = false
		insight::Bool = false
		marlis_brain_boost::Bool = false
		mind_quickening::Bool = false
		netherwind_focus::Bool = false
		obsidian_insight::Bool = false
		spell_blasting::Bool = false
	end
	@kwdef mutable struct Stacks
		ascendance::Integer = 0
		combustion::Integer = 0
		enigmas_answer::Integer = 0
		unstable_power::Integer = 0
	end
	#variables
	@kwdef mutable struct Simulation
		timeline::Vector{Event} = Vector{Event}(undef,1)
		timeline_sorted::Bool = true
		total_time::Real = 0.0
		running::Bool = true
	end
	@kwdef mutable struct Target
		id::Integer = 0
		debuffs::Debuffs = Debuffs()
		damage::Real = 0.0
		level::Integer = enemylevel
	end
	@kwdef mutable struct Mage
		id::Integer = 0
		buffs::Buffs = Buffs()
		cd::Cd = Cd()
		crit_counter::Integer = 0
		current_mana::Real = 0.0
		five_seconds_rule::Bool = false
		oom::Bool = false
		mana_ruby::Bool = true
		s::Stat = Stat()
		stacks::Stacks = Stacks()
		state::Symbol = :idle
		time_tick::Real = 0.5
	end
	function add_event!(time::Real,t::Function,sim::Simulation,mage::Mage)
		insert_event!(Event(sim.total_time + time,mage.id,Dict(:fun => t)),sim)
	end
	function check_cost(cost::Real,mage::Mage)
		if cost <= mage.current_mana
			return true
		else
			return false
		end
	end
	function dodamage!(s::Symbol,mind::Real,maxd::Real,hit::Real,crit::Real,mult::Real,periodic::Bool,binary::Bool,sim::Simulation,mage::Mage,tar::Target)
		local critted::Bool = false
		#check if hit
		if hit >= rand()
			local resist_mult::Real = 1.0-mypdist()
			if binary
				if  resist_mult >= rand()
					#spell not resisted
					resist_mult = 1.0 # binary spell cannot be partial resisted
				else
					@debug "resist"
					# spell fully resisted
					return critted
				end
			end
			#random damage between min and max
			local dmg::Real = unif(mind,maxd)*mult*resist_mult
			if !periodic
				local t::Symbol=get_spell_type(s)
				#periodic spells can't crit
				if crit >= rand()
					#crit is 1.5 times
					dmg *= 1.5
					critted = true
					@debug "critical"
					if t == :fire
						#crit will proc ignite "5/5 ignite" talent
						refresh_ignite!(s,dmg,sim,mage,tar)
					end
				end
				#"5/5 arcane Concentration"
				gain_clearcast!(sim,mage)
				if :WoC in mage.s.special
					gain_spell_blasting!(sim,mage)
				end
				if t == :fire && mage.buffs.combustion
					mage.stacks.combustion += 1
				end
				#consume elemental vulnerability debuff if present
				if tar.debuffs.elemental_vulnerability
					remove_elemental_vulnerability!(sim,mage,tar)
					deleteat!(sim.timeline,findfirst(x -> isequal(x.data[:fun],remove_elemental_vulnerability!),sim.timeline))
				end
				if mage.s.T3 >= 6
					# try to get another elemental vulnerability debuff
					gain_elemental_vulnerability!(hit,sim,mage,tar)
				end
				if :BoeD in mage.s.special
					engulfing_shadows(sim,mage,tar)
				end
			end
			@debug "damage: $(dmg)"
			if resist_mult < 1.0
				@debug "resisted: $(dmg/resist_mult)"
			end
			tar.damage += dmg
		else
			@debug "miss"
			# spell missed
			if mage.s.T2_5 >= 5
				gain_enigmas_answer!(sim,mage)
			end
		end
		return critted
	end
	function end_ascendance!(mage::Mage)
		mage.buffs.ascendance = false
	end
	function end_burst_of_knowledge!(mage::Mage)
		mage.buffs.burst_of_knowledge = false
	end
	function end_cd_burst_of_knowledge!(mage::Mage)
		mage.cd.burst_of_knowledge = false
	end
	function end_cd_clearcast!(mage::Mage)
		mage.cd.clearcast = false
	end
	function end_cd_combustion!(mage::Mage)
		mage.cd.combustion = false
	end
	function end_cd_energy_cloak!(mage::Mage)
		mage.cd.energy_cloak = false
	end
	function end_cd_evocation!(mage::Mage)
		mage.cd.evocation = false
	end
	function end_cd_eye_of_moam!(mage::Mage)
		mage.cd.eye_of_moam = false
	end
	function end_cd_fire_ruby!(mage::Mage)
		mage.cd.fire_ruby = false
	end
	function end_cd_marlis_eye!(mage::Mage)
		mage.cd.marlis_eye = false
	end
	function end_cd_mind_quickening_gem!(mage::Mage)
		mage.cd.mind_quickening_gem = false
	end
	function end_cd_minor_potion!(mage::Mage)
		mage.cd.minor_potion = false
	end
	function end_cd_potion!(mage::Mage)
		mage.cd.potion = false
	end
	function end_cd_robe_of_the_archmage!(mage::Mage)
		mage.cd.robe_of_the_archmage = false
	end
	function end_cd_robes_of_insight!(mage::Mage)
		mage.cd.robes_of_insight = false
	end
	function end_cd_shared_trinket!(mage::Mage)
		mage.cd.shared_trinket = false
	end
	function end_cd_talisman_of_ascendance!(mage::Mage)
		mage.cd.talisman_of_ascendance = false
	end
	function end_cd_talisman_of_ephemeral_power!(mage::Mage)
		mage.cd.talisman_of_ephemeral_power = false
	end
	function end_cd_the_restrained_essence_of_sapphiron!(mage::Mage)
		mage.cd.the_restrained_essence_of_sapphiron = false
	end
	function end_cd_warmth_of_forgiveness!(mage::Mage)
		mage.cd.warmth_of_forgiveness = false
	end
	function end_cd_zandalarian_hero_charm!(mage::Mage)
		mage.cd.zandalarian_hero_charm = false
	end
	function end_chaos_fire!(mage::Mage)
		mage.buffs.chaos_fire = false
	end
	function end_ephemeral_power!(mage::Mage)
		mage.cd.talisman_of_ephemeral_power = false
	end	
	function end_essence_of_sapphiron!(mage::Mage)
		mage.buffs.essence_of_sapphiron = false
	end
	function end_gcd!(mage::Mage)
		mage.cd.gcd = false
	end
	function end_insight!(mage::Mage)
		mage.buffs.insight = false
	end	
	function end_marlis_brain_boost!(mage::Mage)
		mage.buffs.marlis_brain_boost = false
	end	
	function end_mind_quickening!(mage::Mage)
		mage.buffs.mind_quickening = false
	end	
	function end_obsidian_insight!(mage::Mage)
		mage.buffs.obsidian_insight = false
	end
	function end_unstable_power!(mage::Mage)
		mage.stacks.unstable_power = 0
	end	
	function engulfing_shadows(sim::Simulation,mage::Mage,target::Target)
		if rand() <= engulfing_shadows_probability
			gain_mana!(engulfing_shadows_mana,sim,mage)
			_ = dodamage!(:engulfing_shadows,engulfing_shadows_damage,engulfing_shadows_damage,level_to_hit(tar.level),get_crit(mage),get_spell_multipliers(mage,tar),false,true,sim,mage,tar)
		end
	end
	function enter_five_second_rule!(sim::Simulation,mage::Mage)
		if mage.five_seconds_rule
			deleteat!(sim.timeline,index_from_timeline(sim,mage,exit_five_seconds_rule!))
		else
			@debug "$(sim.total_time)\tplayer: $(mage.id)\tentered 5s rule"
			mage.five_seconds_rule=true
		end
		add_event!(5.0,exit_five_seconds_rule!,sim,mage)
	end
	function exit_five_seconds_rule!(sim::Simulation,mage::Mage)
		@debug "$(sim.total_time)\tplayer: $(mage.id)\texited 5s rule"
		mage.five_seconds_rule=false
	end
	function fireball_dot_damage!(sim::Simulation,mage::Mage,tar::Target)
		@debug "$(sim.total_time)\tplayer: $(mage.id)\tfireball dot tick"
		add_event!(2.0,fireball_dot_damage!,sim,mage)
		_ = dodamage!(:fireball_dot,fireball_dot,fireball_dot,get_hit(mage,tar),0.0,get_fire_spell_multipliers(mage,tar),true,false,sim,mage,tar)
	end
	function gain_aura_of_the_blue_dragon!(sim::Simulation,mage::Mage)
		if rand() <= darkmoon_card_blue_dragon_probability
			if mage.buffs.aura_of_the_blue_dragon
				@debug "$(sim.total_time)\tplayer: $(mage.id)\trefreshed aura of the blue dragon"
				sim.timeline[index_from_timeline(sim,mage,remove_aura_of_the_blue_dragon!)].time = sim.total_time + aura_of_the_blue_dragon_duration
				#sim.timeline_sorted = false
			else
				@debug "$(sim.total_time)\tplayer: $(mage.id)\tgained aura of the blue dragon"
				mage.buffs.aura_of_the_blue_dragon = true
				add_event!(aura_of_the_blue_dragon_duration,remove_aura_of_the_blue_dragon!,sim,mage)
			end
		end
	end
	function get_cast_time(spell::Symbol,mage::Mage)
		local time::Real
		if spell == :fireball
			time = fireball_cast
		elseif spell == :scorch
			time = scorch_cast
		elseif spell == :frostbolt
			time = frostbolt_cast
		elseif spell == :flamestrike
			if mage.s.ZG >= 5
				time =flamestrike_cast - 0.5
			else
				time =flamestrike_cast
			end
		end
		if mage.buffs.mind_quickening
			return time / (1.0 + mind_quickening)
		else
			return time
		end
	end
	function gain_clearcast!(sim::Simulation,mage::Mage)
		if arcane_concentration_prob >= rand() && !mage.cd.clearcast
			add_event!(clearcast_duration,remove_clearcast!,sim,mage)
			@debug "$(sim.total_time)\tplayer: $(mage.id)\tclearcast"
			mage.buffs.clearcast = true
			mage.cd.clearcast = true
		end
	end
	function gain_elemental_vulnerability!(hit::Real,sim::Simulation,mage::Mage,tar::Target)
		#elemental vulnerability can be resisted
		if elemental_vulnerability_prob >= rand()
			@debug "$(sim.total_time)\ttarget: $(tar.id)\telemental_vulnerability"
			if hit >= rand()
				tar.debuffs.elemental_vulnerability = true
				add_event!(elemental_vulnerability_duration,remove_elemental_vulnerability!,sim,mage)
			else
				@debug " resisted"
			end
		end
	end
	function gain_enigmas_answer!(sim::Simulation,mage::Mage)
		if mage.stacks.enigmas_answer > 0
			sim.timeline[index_from_timeline(sim,mage,remove_enigmas_answer!)].time = sim.total_time + enigmas_answer_duration
		else
			add_event!(enigmas_answer_duration,remove_enigmas_answer!,sim,mage)
		end
		mage.stacks.enigmas_answer = min(mage.stacks.enigmas_answer+1,enigmas_answer_max_stacks)
		@debug "$(sim.total_time)\tplayer: $(mage.id)\tenigmas's answer stacks: $(mage.stacks.enigmas_answer)"
	end
	function gain_mana!(m::Real,sim::Simulation,mage::Mage)
		mage.current_mana = min(mage.s.max_mana,mage.current_mana + m)
		@debug "$(sim.total_time)\tplayer: $(mage.id)\tmana: $(mage.current_mana)\tΔmana: $(m)"
	end
	function gain_netherwind_focus!(sim::Simulation,mage::Mage)
		if rand() <= netherwind_focus_prob #&& mage.timers.netherwind_focus_cd <= netherwind_focus_cooldown
			@debug "$(sim.total_time)\tplayer: $(mage.id)\tgained netherwind focus"
			mage.buffs.netherwind_focus = true
			add_event!(netherwind_focus_duration,remove_netherwind_focus!,sim,mage)
			#mage.timers.netherwind_focus_cd = 0.0
		end
	end
	function gain_spell_blasting!(sim::Simulation,mage::Mage)
		if spell_blasting_prob >= rand()
			if mage.buffs.spell_blasting
				@debug "$(sim.total_time)\tplayer: $(mage.id)\tspell blasting refreshed"
				sim.timeline[index_from_timeline(sim,mage,remove_spell_blasting!)].time = sim.total_time + spell_blasting_duration
			else
				@debug "$(sim.total_time)\tplayer: $(mage.id)\tspell blasting on"
				mage.buffs.spell_blasting = true
				add_event!(spell_blasting_duration,remove_spell_blasting!,sim,mage)
			end
		end
	end
	function get_discounted_cost(cost::Real,mage::Mage)
		if mage.buffs.clearcast
			return 0.0
		else
			local discount::Float64=0.0
			if mage.buffs.burst_of_knowledge
				discount += 100.0
			end
			if mage.buffs.insight
				discount += insight
			end
			return max(0.0,cost -discount)
		end
	end
	function get_elemental_vulnerability(tar::Target)
		if tar.debuffs.elemental_vulnerability
			return elemental_vulnerability_damage
		else
			return 0.0
		end
	end
	function get_evocation_cooldown(mage::Mage)
		if mage.s.T3 >=2
			return evocation_cooldown -60.0
		else
			return evocation_cooldown
		end
	end
	function get_fire_crit(mage::Mage)
		return min(1.0,mage.s.crit + critical_mass_crit + combustion_crit_per_stack * mage.stacks.combustion)
	end
	function get_fire_spell_multipliers(mage::Mage,tar::Target)
		#"5/5 fire power" talent and "improved scorch"
		return get_spell_multipliers(mage) * (1.0 + fire_power_coeff) * (1.0 + tar.debuffs.scorch_stacks * scorch_stack_coeff)
	end
	function get_frost_crit(mage::Mage)
		return min(1.0,mage.s.crit)
	end
	function get_hit(mage::Mage,tar::Target)
		return min(0.99,level_to_hit(tar.level) + mage.s.hit + mage.stacks.enigmas_answer * enigmas_answer_hit_per_stack)
	end
	function get_mana_tick(mage::Mage)
		local spirit_reg_coeff::Real = 1.0
		if mage.buffs.evocation
			spirit_reg_coeff = 1.6
		else
			if mage.buffs.aura_of_the_blue_dragon
				spirit_reg_coeff = 1.0
			elseif mage.five_seconds_rule
				spirit_reg_coeff = arcane_meditation+mage_armour
			end
		end
		local mpf::Real
		if mage.buffs.drinking
			mpf = mage.s.mp5 + 4200.0/30.0*5.0
		else
			mpf = mage.s.mp5
		end
		return 2.0*(mpf/5.0 + mage.s.spirit_reg*spirit_reg_coeff)
	end
	function get_sp(t::Symbol,mage::Mage)
		local sp::Real = mage.s.sp
		if mage.buffs.spell_blasting
			sp += spell_blasting_sp
		end
		if mage.buffs.ephemeral_power
			sp += ephemeral_power_sp
		end
		if mage.buffs.essence_of_sapphiron
			sp += essence_of_sapphiron_sp
		end
		if mage.buffs.obsidian_insight
			sp += obsidian_insight_sp
		end
		if mage.buffs.chromatic_infusion
			sp += chromatic_infusion_sp
		end
		if mage.buffs.ascendance
			sp += mage.stacks.ascendance * ascendance_sp_per_stack
		end
		sp += mage.stacks.unstable_power * unstable_power_sp_per_stack
		if t == :fire
			return sp + mage.s.firesp
		elseif t == :frost
			return sp + mage.s.frostsp
		else
			return sp
		end
	end	
	function get_spell_multipliers(mage::Mage)
		@static if world_buffs
			return (1.0 + traces_of_silithyst) * (1.0 + sayges_dark_fortune_of_damage)
		else
			return 1.0
		end
	end
	function get_spell_type(s::Symbol)
		if s == :frostbolt
			return :frost
		elseif s == :engulfing_shadows
			return :shadow
		else
			return :fire
		end
	end
	function get_time_to_scorch_debuff_end(sim::Simulation)
		return sim.timeline[findfirst(x -> isequal(x.data[:fun],remove_scorch_debuff!),sim.timeline)].time
	end
	function ignite_tick!(sim::Simulation,mage::Mage,tar::Target)
		@debug "$(sim.total_time)\tplayer: $(mage.id)\tignite tick"
		add_event!(2.0,ignite_tick!,sim,mage)
		#ignite can't crit also doesn't benefit from hit
		_ = dodamage!(:ignite,tar.debuffs.ignite_damage/2.0,tar.debuffs.ignite_damage/2.0,level_to_hit(tar.level),0.0,get_fire_spell_multipliers(mage,tar),true,false,sim,mage,tar)
	end	
	function index_from_timeline(sim::Simulation,mage::Mage,t::Function)
		return findfirst(x -> isequal(x.owner,mage.id) && isequal(x.data[:fun],t),sim.timeline)
	end
	function insert_event!(e::Event,sim::Simulation)
		local i::Union{Integer,Nothing}=findlast(x -> e.time >= x.time,sim.timeline)
		if isnothing(i)
			insert!(sim.timeline,1,e)
		else
			insert!(sim.timeline,1+i,e)
		end
	end
	function level_to_hit(level::Integer)
		if level >= 63
			return 0.83
		elseif level == 62
			return 0.94
		elseif level == 61
			return 0.95
		else
			return 0.96
		end
	end
	function mana_tick!(sim::Simulation,mage::Mage)
		gain_mana!(get_mana_tick(mage),sim,mage)
		insert_event!(Event(sim.total_time+2.0,mage.id,Dict(:fun => mana_tick!)),sim)
	end		
	function marlis_brain_boost_tick!(sim::Simulation,mage::Mage)
		gain_mana!(60.0,sim,mage)
	end
	#custom probability distribution lv 63 (24 resistance)
	function mypdist()
		local a=[0.0,0.25,0.5,0.75]
		return a[StatsBase.sample(StatsBase.Weights([0.82,0.13,0.04,0.01]))]
	end
	function refresh_fireball_dot!(sim::Simulation,mage::Mage,tar::Target)
		if tar.debuffs.fireball_dot[mage.id]
			@debug "$(sim.total_time)\tplayer: $(mage.id)\tfireball dot refresh"
			local ev::Event = popat!(sim.timeline,index_from_timeline(sim,mage,remove_fireball_dot!))
			ev.time = sim.total_time + fireball_dot_duration
			insert_event!(ev,sim)
		else
			@debug "$(sim.total_time)\tplayer: $(mage.id)\tfireball dot"
			add_event!(2.0,fireball_dot_damage!,sim,mage)
			add_event!(fireball_dot_duration,remove_fireball_dot!,sim,mage)
			tar.debuffs.fireball_dot[mage.id] = true
		end
	end
	function refresh_ignite!(s::Symbol,dmg::Real,sim::Simulation,mage::Mage,tar::Target)
		if tar.debuffs.ignite_stacks >= 1
			if tar.debuffs.ignite_stacks < ignite_max_stacks
				tar.debuffs.ignite_stacks += 1
				tar.debuffs.ignite_stacks_type[tar.debuffs.ignite_stacks] = s
				tar.debuffs.ignite_damage += ignite_coeff*dmg
			end
			@debug "$(sim.total_time)\tplayer: $(mage.id)\tignite refresh stacks: $(tar.debuffs.ignite_stacks)"
			local ev::Event = popat!(sim.timeline,findfirst(x -> isequal(x.data[:fun],remove_ignite!),sim.timeline))
			ev.time = sim.total_time + ignite_duration
			insert_event!(ev,sim)
		else
			@debug "$(sim.total_time)\tplayer: $(mage.id)\tignite"
			add_event!(2.0,ignite_tick!,sim,mage)
			add_event!(ignite_duration,remove_ignite!,sim,mage)
			tar.debuffs.ignite_stacks = 1
			tar.debuffs.ignite_stacks_type[1] = s
			tar.debuffs.ignite_damage = ignite_coeff*dmg
		end
	end
	function remove_ascendance!(sim::Simulation,mage::Mage)
		@debug "$(sim.total_time)\tplayer: $(mage.id)\tascendance off"
		mage.buffs.ascendance = false
	end
	function remove_aura_of_the_blue_dragon!(mage::Mage)
		mage.buffs.aura_of_the_blue_dragon = false
	end	
	function remove_clearcast!(sim::Simulation,mage::Mage)
		@debug "$(sim.total_time)\tplayer: $(mage.id)\tclearcast end"
		mage.buffs.clearcast = false
	end
	function remove_combustion!(sim::Simulation,mage::Mage)
		@debug "$(sim.total_time)\tplayer: $(mage.id)\tcombustion end"
		mage.crit_counter = 0
		mage.stacks.combustion = 0
		mage.buffs.combustion = false
		mage.cd.combustion = true
		add_event!(combustion_cooldown,end_cd_combustion!,sim,mage)
	end
	function remove_elemental_vulnerability!(sim::Simulation,mage::Mage,tar::Target)
		@debug "$(sim.total_time)\tplayer: $(mage.id)\telemental vulnerability off"
		tar.debuffs.elemental_vulnerability = false
	end
	function remove_enigmas_answer!(sim::Simulation,mage::Mage)
		@debug "$(sim.total_time)\tplayer: $(mage.id)\tenigma's answer off"
		mage.stacks.enigmas_answer = 0
	end
	function remove_fireball_dot!(sim::Simulation,mage::Mage,tar::Target)
		@debug "$(sim.total_time)\tplayer: $(mage.id)\tfireball dot off"
		tar.debuffs.fireball_dot[mage.id] = false
		deleteat!(sim.timeline,index_from_timeline(sim,mage,fireball_dot_damage!))
	end
	function remove_ignite!(sim::Simulation,tar::Target)
		@debug "$(sim.total_time)\ttarget: $(tar.id)\tignite off"
		tar.debuffs.ignite_stacks = 0
		fill!(tar.debuffs.ignite_stacks_type,nothing)
		deleteat!(sim.timeline,findfirst(x -> isequal(x.data[:fun],ignite_tick!),sim.timeline))
	end
	function remove_netherwind_focus!(sim::Simulation,mage::Mage)
		@debug "$(sim.total_time)\tplayer: $(mage.id)\tlost netherwind focus"
		mage.buffs.netherwind_focus = false
	end
	function remove_scorch_debuff!(sim::Simulation,tar::Target)
		@debug "$(sim.total_time)\ttarget: $(tar.id)\tscorch off"
		tar.debuffs.scorch_stacks = 0
	end
	function remove_spell_blasting!(sim::Simulation,mage::Mage)
		@debug "$(sim.total_time)\tplayer: $(mage.id)\tspell blasting off"
		mage.buffs.spell_blasting = false
	end
	function spend_mana!(cost::Real,sim::Simulation,mage::Mage)
		if mage.buffs.insight
			@debug "$(sim.total_time)\tplayer: $(mage.id)\tinsight off"
			mage.buffs.insight = false
			deleteat!(sim.timeline,index_from_timeline(sim,mage,end_insight!))
		end
		if mage.buffs.clearcast
			@debug "$(sim.total_time)\tplayer: $(mage.id)\tclearcast off"
			mage.buffs.clearcast = false
			deleteat!(sim.timeline,index_from_timeline(sim,mage,remove_clearcast!))
		else
			@debug "$(sim.total_time)\tplayer: $(mage.id)\tmana: $(mage.current_mana)\tΔmana: $(-cost)"
			#can mana go negative?
			mage.current_mana -= cost
		end
	end
	function start_drinking!(sim::Simulation,mage::Mage)
		@debug "$(sim.total_time)\tplayer: $(mage.id)\tdrink start"
		mage.state= :drinking
		mage.buffs.drinking = true
		add_event!(30.0,stop_drinking!,sim,mage)
	end
	function start_scorch!(sim::Simulation,mage::Mage)
		@debug "$(sim.total_time)\tplayer: $(mage.id)\tscorch start"
		mage.state= :casting_scorch
		if mage.buffs.netherwind_focus
			add_event!(0.0,stop_scorch!,sim,mage)
			remove_netherwind_focus!(mage)
		else
			add_event!(get_cast_time(:scorch,mage),stop_scorch!,sim,mage)
		end
		mage.cd.gcd = true
		add_event!(gcd,end_gcd!,sim,mage)
	end
	function start_evocation!(sim::Simulation,mage::Mage)
		@debug "$(sim.total_time)\tplayer: $(mage.id)\tstart evocation"
		mage.state = :evocating
		#evocation will force you out of 5s rule
		if mage.five_seconds_rule
			deleteat!(sim.timeline,index_from_timeline(sim,mage,exit_five_seconds_rule!))
		end
		mage.five_seconds_rule = false
		add_event!(gcd,end_gcd!,sim,mage)
		add_event!(evocation_duration,stop_evocation!,sim,mage)
		add_event!(get_evocation_cooldown(mage::Mage),end_cd_evocation!,sim,mage)
		mage.buffs.evocation = true
		mage.cd.evocation = true
		mage.cd.gcd = true
	end
	function start_fireball!(sim::Simulation,mage::Mage)
		@debug "$(sim.total_time)\tplayer: $(mage.id)\tfireball start"
		mage.state=:casting_fireball
		if mage.buffs.netherwind_focus
			add_event!(0.0,stop_fireball!,sim,mage)
			remove_netherwind_focus!(mage)
		else
			add_event!(get_cast_time(:fireball,mage),stop_fireball!,sim,mage)
		end
		mage.cd.gcd = true
		add_event!(gcd,end_gcd!,sim,mage)
	end
	function start_flamestrike!(sim::Simulation,mage::Mage)
		@debug "$(sim.total_time)\tplayer: $(mage.id)\tflamestrike start"
		mage.state=:casting_flamestrike
		if mage.buffs.netherwind_focus
			add_event!(0.0,stop_flamestrike!,sim,mage)
			remove_netherwind_focus!(mage)
		else
			add_event!(get_cast_time(:flamestrike,mage),stop_flamestrike!,sim,mage)
		end
		mage.cd.gcd = true
		add_event!(gcd,end_gcd!,sim,mage)
	end
	function start_frostbolt!(sim::Simulation,mage::Mage)
		@debug "$(sim.total_time)\tplayer: $(mage.id)\tfrostbolt start"
		mage.state=:casting_frostbolt
		if mage.buffs.netherwind_focus
			add_event!(0.0,stop_frostbolt!,sim,mage)
			remove_netherwind_focus!(mage)
		else
			add_event!(get_cast_time(:frostbolt,mage),stop_frostbolt!,sim,mage)
		end
		mage.cd.gcd = true
		add_event!(gcd,end_gcd!,sim,mage)
	end
	function state_machine!(sim::Simulation,mages::Vector{Mage},tar::Target)
		sim.timeline[1] =Event(sim.total_time + 2.0, mages[1].id,Dict(:fun => mana_tick!))
		for i in 2:length(mages)
			push!(sim.timeline,Event(sim.total_time + 2.0, mages[i].id,Dict(:fun => mana_tick!)))
		end
		while !reduce(&,[m.oom for m in mages])
			for i in 1:length(mages)
				if !mages[i].cd.gcd && mages[i].state == :idle
					if :RotA in mages[i].s.special && !mages[i].cd.robe_of_the_archmage && (mages[i].s.max_mana - mages[i].current_mana) > robe_of_the_archmage_max
						use_robe_of_the_archmage!(sim,mages[i])
					end
					if :EC in mages[i].s.special && !mages[i].cd.energy_cloak && (mages[i].s.max_mana - mages[i].current_mana) > energy_cloak_max
						use_energy_cloak!(sim,mages[i])
					end
					if mages[i].mana_ruby && !mages[i].cd.minor_potion && (mages[i].s.max_mana - mages[i].current_mana) > mana_ruby_max
						use_mana_ruby!(sim,mages[i])
					end
					@static if rotation != :sapphiron
						if !mages[i].cd.minor_potion && (mages[i].s.max_mana - mages[i].current_mana) > demonic_rune_max
							use_demonic_rune!(sim,mages[i])
						end
						if !mages[i].cd.potion && (mages[i].s.max_mana - mages[i].current_mana) > major_mana_potion_max
							use_major_mana_potion!(sim,mages[i])
						end
					end
					if tar.debuffs.scorch_stacks >= 1
						local t::Real = get_time_to_scorch_debuff_end(sim)
					end
					@static if fireimmune
						if check_cost(get_discounted_cost(frostbolt_cost,mages[i]),mages[i])
							use_trinket1!(mages[i])
							use_trinket2!(mages[i])
							if :RoI in mages[i].s.special && !mages[i].cd.robes_of_insight
								use_robes_of_insight!(sim,mages[i])
							end
							start_frostbolt!(sim,mages[i])
						else
							#no mana, time to evocate
							if !mages[i].cd.evocation
								start_evocation!(sim,mages[i])
							else
								#no mana, evocation on cooldown
								mages[i].oom=true
							end
						end
					elseif rotation == :naxxramastrash
						if check_cost(get_discounted_cost(flamestrike_cost,mages[i]),mages[i])
							use_trinket1!(mages[i])
							use_trinket2!(mages[i])
							if :RoI in mages[i].s.special && !mages[i].cd.robes_of_insight
								use_robes_of_insight!(sim,mages[i])
							end
							start_flamestrike!(sim,mages[i])
						else
							#no mana, time to evocate
							if !mages[i].cd.evocation
								start_evocation!(sim,mages[i])
							else
								#no mana, evocation on cooldown
								mages[i].oom=true
							end
						end
					elseif !fireimmune
						if check_cost(get_discounted_cost(scorch_cost,mages[i]),mages[i]) && (tar.debuffs.scorch_stacks < scorch_max_stacks || (sim.total_time < t -scorch_cast && sim.total_time >= t -scorch_cast -fireball_cast))
							start_scorch!(sim,mages[i])
						else
							#drop ignite
							if tar.debuffs.ignite_stacks > 0 && :scorch in tar.debuffs.ignite_stacks_type
								#TODO: sapphiron
								start_frostbolt!(sim,mages[i])
							end
							#combustion available
							if !mages[i].cd.combustion && !mages[i].buffs.combustion
								use_combustion!(sim,mages[i])
							end
							use_trinket1!(mages[i])
							use_trinket2!(mages[i])
							if :RoI in mages[i].s.special && !mages[i].cd.robes_of_insight
								use_robes_of_insight!(sim,mages[i])
							end
							if check_cost(get_discounted_cost(fireball_cost,mages[i]),mages[i])
								start_fireball!(sim,mages[i])
							else
								#no mana, time to evocate
								if !mages[i].cd.evocation
									start_evocation!(sim,mages[i])
								else
									#no mana, evocation on cooldown
									mages[i].oom=true 
	#								start_drinking!(sim,mages[i])
								end
							end
						end
					end
				end
			end
			local current::Event = popfirst!(sim.timeline)
			sim.total_time=current.time
			local i=current.owner
			if applicable(current.data[:fun],mages[i])
				current.data[:fun](mages[i])
			elseif applicable(current.data[:fun],sim,mages[i])
				current.data[:fun](sim,mages[i])
			elseif applicable(current.data[:fun],sim,mages[i],tar)
				current.data[:fun](sim,mages[i],tar)
			elseif applicable(current.data[:fun],sim,tar)
				current.data[:fun](sim,tar)
			end
			#println(sim.timeline)
		end
		return tar.damage/sim.total_time,sim.total_time
	end
	function stop_drinking!(mage::Mage)
		mage.buffs.drinking = false
		mage.state = :idle
	end	
	function stop_evocation!(sim::Simulation,mage::Mage)
		@debug "$(sim.total_time)\tplayer: $(mage.id)\tstop evocation"
		mage.buffs.evocation = false
		mage.state = :idle
	end
	function stop_scorch!(sim::Simulation,mage::Mage,tar::Target)
		@debug "$(sim.total_time)\tplayer: $(mage.id)\tscorch end"
		spend_mana!(get_discounted_cost(scorch_cost,mage),sim,mage)
		mage.state= :idle
		enter_five_second_rule!(sim,mage)
		local temp::Real = scorch_coeff * (get_sp(:fire,mage) + get_elemental_vulnerability(tar))
		local min_dmg::Real = scorch_min + temp
		local max_dmg::Real = scorch_max + temp
		local hit::Real=get_hit(mage,tar)
		local scorch_critted::Bool = dodamage!(:scorch,min_dmg,max_dmg,hit,get_fire_crit(mage),get_fire_spell_multipliers(mage,tar),false,false,sim,mage,tar)
		update_trinket_buffs!(mage)
		if scorch_critted
			#recover mana on crit "3/3 masters of elements" talent
			gain_mana!(master_of_elements_coeff*scorch_cost,sim,mage)
			update_combustion!(sim,mage)
		end
		#"3/3" improved scorch talent
		#debuff can be hit separatedly from the damage component
		if hit >= rand()
			update_scorch_stacks!(sim,mage,tar)
		else
			@debug "$(sim.total_time)\tplayer: $(mage.id)\tscorch debuff resisted"
		end
		if :DCBD in mage.s.special
			gain_aura_of_the_blue_dragon!(sim,mage)
		end
	end
	function stop_fireball!(sim::Simulation,mage::Mage,tar::Target)
		@debug "$(sim.total_time)\tplayer: $(mage.id)\tfireball end"
		spend_mana!(get_discounted_cost(fireball_cost,mage),sim,mage)
		mage.state=:idle
		enter_five_second_rule!(sim,mage)
		if :DCBD in mage.s.special
			gain_aura_of_the_blue_dragon!(sim,mage)
		end
		if mage.s.T2 >= 8
			gain_netherwind_focus!(mage)
		end
		local temp::Real = fireball_coeff * (get_sp(:fire,mage) + get_elemental_vulnerability(tar))
		local min_dmg::Real = fireball_min + temp
		local max_dmg::Real = fireball_max + temp
		local fireball_critted::Bool = dodamage!(:fireball,min_dmg,max_dmg,get_hit(mage,tar),get_fire_crit(mage),get_fire_spell_multipliers(mage,tar),false,false,sim,mage,tar)
		refresh_fireball_dot!(sim,mage,tar)
		update_trinket_buffs!(mage)
		if fireball_critted
			#recover mana on crit "3/3 masters of elements" talent
			gain_mana!(master_of_elements_coeff*fireball_cost,sim,mage)
			update_combustion!(sim,mage)
		end
	end
	function stop_flamestrike!(sim::Simulation,mage::Mage,tar::Target)
		@debug "$(sim.total_time)\tplayer: $(mage.id)\tflamestrike end"
		spend_mana!(get_discounted_cost(flamestrike_cost,mage),sim,mage)
		mage.state=:idle
		enter_five_second_rule!(sim,mage)
		if :DCBD in mage.s.special
			gain_aura_of_the_blue_dragon!(sim,mage)
		end
		local temp::Real = flamestrike_coeff * (get_sp(:fire,mage) + get_elemental_vulnerability(tar))
		local min_dmg::Real = flamestrike_min + temp
		local max_dmg::Real = flamestrike_max + temp
		local flamestrike_critted::Bool = dodamage!(:flamestrike,min_dmg,max_dmg,get_hit(mage,tar),get_fire_crit(mage),get_fire_spell_multipliers(mage,tar),false,false,sim,mage,tar)
		refresh_flamestrike_dot!(mage)
		update_trinket_buffs!(mage)
		if flamestrike_critted
			#recover mana on crit "3/3 masters of elements" talent
			gain_mana!(master_of_elements_coeff*flamestrike_cost,sim,mage)
			update_combustion!(sim,mage)
		end
	end
	function stop_frostbolt!(sim::Simulation,mage::Mage,tar::Target)
		@debug "$(sim.total_time)\tplayer: $(mage.id)\tfrostbolt end"
		spend_mana!(get_discounted_cost(frostbolt_cost,mage),sim,mage)
		mage.state=:idle
		enter_five_second_rule!(sim,mage)
		if :DCBD in mage.s.special
			gain_aura_of_the_blue_dragon!(sim,mage)
		end
		if mage.s.T2 >= 8
			gain_netherwind_focus!(mage)
		end
		local temp::Real = frostbolt_coeff * (get_sp(:frost,mage) + get_elemental_vulnerability(tar))
		local min_dmg::Real = frostbolt_min + temp
		local max_dmg::Real = frostbolt_max + temp
		local frostbolt_critted::Bool = dodamage!(:frostbolt,min_dmg,max_dmg,get_hit(mage,tar),get_frost_crit(mage),get_spell_multipliers(mage),false,true,sim,mage,tar)
		update_trinket_buffs!(mage)
		if frostbolt_critted
			#recover mana on crit "3/3 masters of elements" talent
			gain_mana!(master_of_elements_coeff*frostbolt_cost,sim,mage)
		end
	end
	function unif(xmin::Real,xmax::Real)
		return xmin + rand()*(xmax - xmin)
	end		
	function update_combustion!(sim::Simulation,mage::Mage)
		if mage.buffs.combustion
			mage.crit_counter += 1
			if mage.crit_counter >= 3
				remove_combustion!(sim,mage)
			end
		end
	end
	function update_scorch_stacks!(sim::Simulation,mage::Mage,tar::Target)
		if tar.debuffs.scorch_stacks > 0
			local ev::Event = popat!(sim.timeline,findfirst(x -> isequal(x.data[:fun],remove_scorch_debuff!),sim.timeline))
			ev.time = sim.total_time + scorch_debuff_duration
			insert_event!(ev,sim)
		else
			add_event!(scorch_debuff_duration,remove_scorch_debuff!,sim,mage)
		end
		tar.debuffs.scorch_stacks = min(scorch_max_stacks,tar.debuffs.scorch_stacks + 1)
		@debug "$(sim.total_time)\tplayer: $(mage.id)\tscorch stacks: $(tar.debuffs.scorch_stacks)"
	end
	function update_trinket_buffs!(mage::Mage)
		if mage.stacks.unstable_power > 0
			mage.stacks.unstable_power -= 1
			@debug "$(sim.total_time)\tplayer: $(mage.id)\tunstable power stacks: $(mage.stacks.unstable_power)"
		end
		if mage.buffs.ascendance
			mage.stacks.ascendance += 1
			@debug "$(sim.total_time)\tplayer: $(mage.id)\tascendance stacks: $(mage.stacks.ascendance)"
		end
	end
	function use_burst_of_knowledge!(sim::Simulation,mage::Mage)
		@debug "$(sim.total_time)\tplayer: $(mage.id)\tBurst of Knowledge used"
		add_event!(burst_of_knowledge_duration,end_burst_of_knowledge!,sim,mage)
		add_event!(burst_of_knowledge_cooldown,end_cd_burst_of_knowledge!,sim,mage)
		mage.buffs.burst_of_knowledge = true
		mage.cd.burst_of_knowledge = true
	end	
	function use_combustion!(sim::Simulation,mage::Mage)
		@debug "$(sim.total_time)\tplayer: $(mage.id)\tcombustion start"
		mage.buffs.combustion = true
		mage.stacks.combustion = 1
		mage.crit_counter = 0
	end
	function use_demonic_rune!(sim::Simulation,mage::Mage)
		@debug "$(sim.total_time)\tplayer: $(mage.id)\tdemonic rune"
		gain_mana!(unif(demonic_rune_min,demonic_rune_max),sim,mage)
		add_event!(minor_potion_cooldown,end_cd_minor_potion!,sim,mage)
		mage.cd.minor_potion = true
	end
	function use_energy_cloak!(sim::Simulation,mage::Mage)
		@debug "$(sim.total_time)\tplayer: $(mage.id)\tenergy cloak"
		gain_mana!(unif(energy_cloak_min,energy_cloak_max),sim,mage)
		add_event!(energy_cloak_cd,end_cd_energy_cloak!,sim,mage)
		mage.cd.energy_cloak = true
	end
	function use_eye_of_moam!(sim::Simulation,mage::Mage)
		@debug "$(sim.total_time)\tplayer: $(mage.id)\tEye of Moam trinket used"
		mage.buffs.obsidian_insight = true
		add_event!(shared_trinket_cooldown,end_cd_shared_trinket!,sim,mage)
		add_event!(obsidian_insight_duration,end_obsidian_insight!,sim,mage)
		add_event!(eye_of_moam_cooldown,end_cd_eye_of_moam!,sim,mage)
		mage.cd.eye_of_moam = true
		mage.cd.shared_trinket = true
	end
	function use_fire_ruby!(sim::Simulation,mage::Mage)
		@debug "$(sim.total_time)\tplayer: $(mage.id)\tFire Ruby trinket used"
		gain_mana!(unif(fire_ruby_min,fire_ruby_max),sim,mage)
		mage.buffs.chaos_fire = true
		add_event!(shared_trinket_cooldown,end_cd_shared_trinket!,sim,mage)
		add_event!(chaos_fire_duration,end_chaos_fire!,sim,mage)
		add_event!(fire_ruby_cooldown,end_cd_fire_ruby!,sim,mage)
		mage.cd.fire_ruby = true
		mage.cd.shared_trinket = true
	end
	function use_major_mana_potion!(sim::Simulation,mage::Mage)
		@debug "$(sim.total_time)\tplayer: $(mage.id)\tmajor mana potion"
		gain_mana!(unif(major_mana_potion_min,major_mana_potion_max),sim,mage)
		add_event!(potion_cooldown,end_cd_potion!,sim,mage)
		mage.cd.potion = true
	end
	function use_mana_ruby!(sim::Simulation,mage::Mage)
		@debug "$(sim.total_time)\tplayer: $(mage.id)\tmana ruby"
		gain_mana!(unif(mana_ruby_min,mana_ruby_max),sim,mage)
		add_event!(minor_potion_cooldown,end_cd_minor_potion!,sim,mage)
		mage.cd.minor_potion = true
		mage.mana_ruby = false
	end
	function use_marlis_eye!(sim::Simulation,mage::Mage)
		@debug "$(sim.total_time)\tplayer: $(mage.id)\tMar'li's Eye trinket used"
		mage.buffs.marlis_brain_boost = true
		for n::Integer in 1:6
			add_event!(n*5.0,marlis_brain_boost_tick!,sim,mage)
		end
		add_event!(marlis_brain_boost_duration,end_marlis_brain_boost!,sim,mage)
		add_event!(marlis_eye_cooldown,end_cd_marlis_eye!,sim,mage)
		mage.cd.marlis_eye = true
	end
	function use_mind_quickening_gem!(sim::Simulation,mage::Mage)
		@debug "$(sim.total_time)\tplayer: $(mage.id)\tMind Quickening Gem trinket used"
		mage.buffs.mind_quickening = true
		add_event!(shared_trinket_cooldown,end_cd_shared_trinket!,sim,mage)
		add_event!(mind_quickening_duration,end_mind_quickening!,sim,mage)
		add_event!(mind_quickening_gem_cooldown,end_cd_mind_quickening_gem!,sim,mage)
		mage.cd.mind_quickening_gem = true
		mage.cd.shared_trinket = true
	end
	function use_robe_of_the_archmage!(sim::Simulation,mage::Mage)
		@debug "$(sim.total_time)\tplayer: $(mage.id)\trobe of the archmage"
		gain_mana!(unif(robe_of_the_archmage_min,robe_of_the_archmage_max),sim,mage)
		add_event!(robe_of_the_archmage_cooldown,end_cd_robe_of_the_archmage!,sim,mage)
		mage.cd.robe_of_the_archmage = true
	end
	function use_robes_of_insight!(sim::Simulation,mage::Mage)
		@debug "$(sim.total_time)\tplayer: $(mage.id)\trobes of insight"
		add_event!(insight_duration,end_insight!,sim,mage)
		add_event!(robes_of_insight_cooldown,end_cd_robes_of_insight!,sim,mage)
		mage.cd.robes_of_insight = true
		mage.buffs.insight=true
	end
	function use_talisman_of_ascendance!(sim::Simulation,mage::Mage)
		@debug "$(sim.total_time)\tplayer: $(mage.id)\tTalisman of Ascendance trinket used"
		mage.buffs.ascendance = true
		mage.stacks.ascendance = 0
		add_event!(shared_trinket_cooldown,end_cd_shared_trinket!,sim,mage)
		add_event!(ascendance_duration,end_ascendance!,sim,mage)
		add_event!(talisman_of_ascendance_cooldown,end_cd_talisman_of_ascendance!,sim,mage)
		mage.cd.talisman_of_ascendance = true
		mage.cd.shared_trinket = true
	end
	function use_talisman_of_ephemeral_power!(sim::Simulation,mage::Mage)
		@debug "$(sim.total_time)\tplayer: $(mage.id)\tTalisman of Ephemeral Power trinket used"
		mage.buffs.ephemeral_power = true
		add_event!(shared_trinket_cooldown,end_cd_shared_trinket!,sim,mage)
		add_event!(ephemeral_power_duration,end_ephemeral_power!,sim,mage)
		add_event!(talisman_of_ephemeral_power_cooldown,end_cd_talisman_of_ephemeral_power!,sim,mage)
		mage.cd.talisman_of_ephemeral_power = true
		mage.cd.shared_trinket = true
	end
	function use_the_restrained_essence_of_sapphiron!(sim::Simulation,mage::Mage)
		@debug "$(sim.total_time)\tplayer: $(mage.id)\tThe Restrained Essence of Sapphiron used"
		mage.buffs.essence_of_sapphiron = true
		add_event!(shared_trinket_cooldown,end_cd_shared_trinket!,sim,mage)
		add_event!(essence_of_sapphiron_duration,end_essence_of_sapphiron!,sim,mage)
		add_event!(the_restrained_essence_of_sapphiron_cooldown,end_cd_the_restrained_essence_of_sapphiron!,sim,mage)
		mage.cd.the_restrained_essence_of_sapphiron = true
		mage.cd.shared_trinket = true
	end
	function use_trinket!(t::Symbol,mage::Mage)
		if t == :BoK && !mage.cd.burst_of_knowledge
			use_burst_of_knowledge!(mage)
		elseif t == :MLE && !mage.cd.marlis_eye
			use_marlis_eye!(mage)
		end
		if !mage.cd.shared_trinket
			# trinket here share cooldown
			if t == :EoM && !mage.cd.eye_of_moam
				use_eye_of_moam!(mage)
			elseif t == :FR && !mage.cd.fire_ruby
				use_fire_ruby!(mage)
			elseif t == :MQG && !mage.cd.mind_quickening_gem
				use_mind_quickening_gem!(mage)
			elseif t == :ToA && !mage.cd.talisman_of_ascendance
				use_talisman_of_ascendance!(mage)
			elseif t == :ToEP && !mage.cd.talisman_of_ephemeral_power
				use_talisman_of_ephemeral_power!(mage)
			elseif t == :TREoS && !mage.cd.the_restrained_essence_of_sapphiron
				use_the_restrained_essence_of_sapphiron!(mage)
			elseif t == :WoF && !mage.cd.warmth_of_forgiveness
				use_warmth_of_forgiveness!(mage)
			elseif t == :ZHC && !mage.cd.zandalarian_hero_charm
				use_zandalarian_hero_charm!(mage)
			end
		end
	end
	function use_trinket1!(mage::Mage)
		use_trinket!(mage.s.trinket1,mage::Mage)
	end
	function use_trinket2!(mage::Mage)
		use_trinket!(mage.s.trinket2,mage::Mage)
	end
	function use_warmth_of_forgiveness!(sim::Simulation,mage::Mage)
		@debug "$(sim.total_time)\tplayer: $(mage.id)\tWarmth of Forgiveness used"
		gain_mana!(warmth_of_forgiveness,sim,mage)
		add_event!(shared_trinket_cooldown,end_cd_shared_trinket!,sim,mage)
		add_event!(warmth_of_forgiveness_cooldown,end_cd_warmth_of_forgiveness!,sim,mage)
		mage.cd.warmth_of_forgiveness = true
		mage.cd.shared_trinket = true
	end
	function use_zandalarian_hero_charm!(sim::Simulation,mage::Mage)
		@debug "$(sim.total_time)\tplayer: $(mage.id)\tZandalarian Hero Charm used"
		mage.stacks.unstable_power = unstable_power_max_stacks
		add_event!(shared_trinket_cooldown,end_cd_shared_trinket!,sim,mage)
		add_event!(unstable_power_duration,end_unstable_power!,sim,mage)
		add_event!(zandalarian_hero_charm_cooldown,end_cd_zandalarian_hero_charm!,sim,mage)
		mage.cd.zandalarian_hero_charm = true
		mage.cd.shared_trinket = true
	end
end
