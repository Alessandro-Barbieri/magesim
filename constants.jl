#     Copyright (C) 2013  Alessandro Barbieri
# 
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU Affero General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
# 
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU Affero General Public License for more details.
# 
#     You should have received a copy of the GNU Affero General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.

const rotation::Symbol = :sapphiron
#const rotation::Symbol = :instance

if rotation == :sapphiron
	const frostimmune::Bool = true
	const fireimmune::Bool = false
	const undeadenemy::Bool = true
	const enemylevel::Int = 63
	const raid_buffs::Bool = true
	const world_buffs::Bool = true
	const consumables::Bool = true
	const nplayers::Int = 5
	const nenemies::Int = 1
elseif rotation == :naxxramasboss
	const frostimmune::Bool = false
	const fireimmune::Bool = false
	const undeadenemy::Bool = true
	const enemylevel::Int = 63
	const raid_buffs::Bool = true
	const world_buffs::Bool = true
	const consumables::Bool = true
	const nplayers::Int = 5
	const nenemies::Int = 1
elseif rotation == :naxxramastrash
	const frostimmune::Bool = false
	const fireimmune::Bool = false
	const undeadenemy::Bool = true
	const enemylevel::Int = 61
	const raid_buffs::Bool = false
	const world_buffs::Bool = false
	const consumables::Bool = false
	const nplayers::Int = 5
	const nenemies::Int = 9
elseif rotation == :bwldragons
	const frostimmune::Bool = false
	const fireimmune::Bool = true
	const undeadenemy::Bool = false
	const enemylevel::Int = 63
	const raid_buffs::Bool = true
	const world_buffs::Bool = true
	const consumables::Bool = true
	const nplayers::Int = 5
	const nenemies::Int = 1
elseif rotation == :onyxia
	const frostimmune::Bool = false
	const fireimmune::Bool = true
	const undeadenemy::Bool = false
	const enemylevel::Int = 63
	const raid_buffs::Bool = true
	const world_buffs::Bool = false
	const consumables::Bool = true
	const nplayers::Int = 5
	const nenemies::Int = 1
elseif rotation == :fire63
	const frostimmune::Bool = false
	const fireimmune::Bool = false
	const undeadenemy::Bool = false
	const enemylevel::Int = 63
	const raid_buffs::Bool = true
	const world_buffs::Bool = true
	const consumables::Bool = true
	const nplayers::Int = 5
	const nenemies::Int = 1
else
	const frostimmune::Bool = false
	const fireimmune::Bool = false
	const undeadenemy::Bool = false
	const enemylevel::Int = 60
	const raid_buffs::Bool = false
	const world_buffs::Bool = false
	const consumables::Bool = false
	const nplayers::Int = 1
	const nenemies::Int = 1
end

#constants
const arcane_intellect::Float64 = 31.0
const arcane_meditation_2::Float64 = 2.0*0.05
const arcane_meditation::Float64 = 3.0*0.05
const arcane_concentration_prob::Float64 = 0.1
const ascendance_max_spells::Int = 6
const ascendance_max_stacks::Int = 5
const ascendance_sp_per_stack::Float64 = 40.0
const base_int::Float64 = 18.0+3.0
const base_mana::Float64 = 1213.0
const base_spi::Float64 = 25.0+2.0
const brilliant_wizard_oil_crit::Float64 = 0.01
const brilliant_wizard_oil_sp::Float64 = 36.0
const chromatic_infusion_sp::Float64 = 50.0
const combustion_crit_per_stack::Float64 = 0.1
const critical_mass_crit::Float64 = 0.05
const darkmoon_card_blue_dragon_probability::Float64 = 0.02
const demonic_rune_min::Float64 = 900.0
const demonic_rune_max::Float64 = 1500.0
const elemental_precision_2::Float64 = 2.0*0.02
const elemental_precision::Float64 = 3.0*0.02
const elemental_vulnerability_damage::Float64 = 200.0
const elemental_vulnerability_prob::Float64 = 0.2
const elixir_of_greater_firepower::Float64 = 40.0
const elixir_of_frost_power::Float64 = 15.0
const energy_cloak_cd::Float64 = 60.0*60.0
const energy_cloak_min::Float64 = 375.0
const energy_cloak_max::Float64 = 425.0
const engulfing_shadows_damage::Float64 = 100.0
const engulfing_shadows_mana::Float64 = 100.0
const engulfing_shadows_probability::Float64 = 0.1
const enigmas_answer_max_stacks::Int = 5
const enigmas_answer_hit_per_stack::Float64 = 0.05
const ephemeral_power_sp::Float64 = 175.0
const essence_of_sapphiron_sp::Float64 = 130.0
const fire_power_coeff::Float64 = 0.1
const fire_ruby_max::Float64 = 500.0
const fire_ruby_min::Float64 = 1.0
const fireball_coeff::Float64 = 1.0
const fireball_cost::Float64 = 410.0
const fireball_dot::Float64 = 19.0
const fireball_max::Float64 = 760.0
const fireball_min::Float64 = 596.0
const flask_of_supreme_power::Float64 = 150.0
const flamestrike_cast::Float64 = 3.0
const flamestrike_coeff::Float64=0.157
const flamestrike_dot_coeff::Float64=0.02
const flamestrike_cost::Float64 = 990.0
const flamestrike_max::Float64 =466.0
const flamestrike_min::Float64 = 381.0
const flamestrike_dot::Float64=85.0
const flamestrike_r5_cost::Float64 = 815.0
const flamestrike_r5_max::Float64 =367.0
const flamestrike_r5_min::Float64 = 298.0
const flamestrike_r5_dot::Float64=66.0
const frost_resistance_totem::Float64 = 60.0
const frostbolt_coeff::Float64 = 0.814
const frostbolt_cost::Float64 = 290
const frostbolt_cast::Float64 = 3.0
const frostbolt_max::Float64 = 555.0
const frostbolt_min::Float64 = 515.0
const gcd::Float64 = 1.5
const gift_of_arthas::Float64 = 10.0
const greater_arcane_elixir::Float64 = 35.0
const ignite_coeff::Float64 = 0.4
const ignite_max_stacks::Int = 5
const improved_fireball::Float64 = 0.5
const insight::Float64 = 500.0
const insight_duration::Float64 = 10.0
const juju_chill::Float64 = 10.0
const kreegs_stout_beatdown_int::Float64 = -5.0
const kreegs_stout_beatdown_spi::Float64 = 25.0
const mage_armour::Float64 = 0.3
const mage_armor_resistance::Float64 = 15.0
const mageblood_potion::Float64 = 12.0
const major_mana_potion_max::Float64 = 2250.0
const major_mana_potion_min::Float64 = 1350.0
const mana_ruby_max::Float64 = 1200.0
const mana_ruby_min::Float64 = 1000.0
const mana_spring_totem_mp5::Float64 = 15.0
const mark_of_the_wild::Float64 = 12.0*1.35
const mark_of_the_wild_resistances::Float64 = 20.0*1.35
const master_of_elements_coeff::Float64 = 0.3
const mind_quickening::Float64 = 30.0/100.0
const moonkin_aura::Float64 = 0.03
const netherwind_focus_max_cast::Float64 = 10.0
const netherwind_focus_prob::Float64 = 10.0
const obsidian_insight_sp::Float64 = 50.0
const power_of_the_guardian_druid::Float64 = 11.0
const power_of_the_guardian_mage::Float64 = 0.02
const power_of_the_guardian_warlock::Float64 = 33.0
const prayer_of_spirit::Float64 = 40.0
const rallying_cry_of_the_dragonslayer::Float64 = 0.1
const robe_of_the_archmage_max::Float64 = 375.0
const robe_of_the_archmage_min::Float64 = 625.0
const runn_tum_tuber_surprise::Float64 = 10.0
const sayges_dark_fortune_of_damage::Float64 = 0.1
const scorch_cast::Float64 = 1.5
const scorch_coeff::Float64 = 0.429
const scorch_cost::Float64 = 150.0
const scorch_max::Float64 = 280.0
const scorch_max_stacks::Int = 5
const scorch_min::Float64 = 237.0
const scorch_stack_coeff::Float64 = 0.03
const shadow_protection::Float64 = 60.0
const shadow_resistance::Float64 = 10.0
const slipkiks_savvy::Float64 = 0.03
const songflower_serenade::Float64 = 15.0
const songflower_serenade_crit::Float64 = 0.05
const spell_blasting_prob::Float64 = 0.05
const spell_blasting_sp::Float64 = 132.0
const spirit_of_zandalar::Float64 = 1.15
const spirit_of_zanza::Float64 = 50.0
const traces_of_silithyst::Float64 = 0.05
const unstable_power_max_stacks::Int = 12
const unstable_power_sp_per_stack::Float64 = 17.0
const warchiefs_blessing::Float64 = 10.0
const warmth_of_forgiveness::Float64 = 500.0

#cooldowns
#const netherwind_focus_cooldown::Float64 = 10.0
const burst_of_knowledge_cooldown::Float64 = 15*60.0
const clearcast_cooldown::Float64 = 1.0
const combustion_cooldown::Float64 = 3.0*60.0
const evocation_cooldown::Float64 = 8.0*60.0
const eye_of_moam_cooldown::Float64 = 3*60.0
const fire_ruby_cooldown::Float64 = 3*60.0
const marlis_eye_cooldown::Float64 = 3*60.0
const mind_quickening_gem_cooldown::Float64 = 5*60.0
const minor_potion_cooldown::Float64 = 2.0*60
const potion_cooldown::Float64 = 2.0*60.0
const robe_of_the_archmage_cooldown::Float64 = 5.0*60.0
const robes_of_insight_cooldown::Float64 = 15.0*60.0
const shared_trinket_cooldown::Float64 = 30.0
const talisman_of_ascendance_cooldown::Float64 = 60.0
const talisman_of_ephemeral_power_cooldown::Float64 = 90.0
const the_restrained_essence_of_sapphiron_cooldown::Float64 = 2*60.0
const warmth_of_forgiveness_cooldown::Float64 = 3*60.0
const zandalarian_hero_charm_cooldown::Float64 = 2*60.0

#durations
const aura_of_the_blue_dragon_duration::Float64 =15.0
const burst_of_knowledge_duration::Float64 = 10.0
const clearcast_duration::Float64 = 15.0
const elemental_vulnerability_duration::Float64 = 30.0
const enigmas_answer_duration::Float64 = 20.0
const ephemeral_power_duration::Float64 = 15.0
const essence_of_sapphiron_duration::Float64 = 20.0
const evocation_duration::Float64 = 8.0
const fireball_dot_duration::Float64 = 8.0
const flamestrike_dot_duration::Float64=8.0
const ignite_duration::Float64 = 4.0
const marlis_brain_boost_duration::Float64 = 30.0
const mind_quickening_duration::Float64 = 20.0
const netherwind_focus_duration::Float64 = 10.0
const obsidian_insight_duration::Float64 = 30.0
const scorch_debuff_duration::Float64 = 30.0
const spell_blasting_duration::Float64 = 10.0
const unstable_power_duration::Float64 = 20.0

const fireball_cast::Float64 = 3.5 - improved_fireball
