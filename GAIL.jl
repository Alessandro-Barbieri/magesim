#     Copyright (C) 2013  Alessandro Barbieri
# 
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU Affero General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
# 
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU Affero General Public License for more details.
# 
#     You should have received a copy of the GNU Affero General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.

module GAIL

import Statistics
import NLopt
using ArgCheck
import Distributions

function κmax(α::Real,n::Real,ℭ::Real)
	return (n-3)/(n-1)+(α*n)/(1.0-α)*(1.0-1.0/ℭ)
end

function α_t(t::Real,α::Real,α_σ::Real)
	return (α-α_σ)/(1.0-α_σ)*2.0^(-t)
end

function meanMC_g(
	f::Function,
	ε_a::Real,
	ε_r::Real;
	α::Real = 0.01,
	α_σ::Real = α / 2.0,
	n_σ::Integer = 10000,
	n_1::Integer = 10000,
	ℭ::Real = 1.2,
	θ::Real = 0.95,
	tol::Function = max
)

	@argcheck ε_a + ε_r > 0.0
	@argcheck ε_a >= 0.0
	@argcheck ε_r >= 0.0
	@argcheck ε_r < 1.0
	@argcheck α > 0.0
	@argcheck α < 1.0
	@argcheck θ < 1.0
	@argcheck 0.0 < θ
	@argcheck α > α_σ
	@argcheck 0.0 < α_σ
	@argcheck n_σ >= 30
	@argcheck n_1 >= 30
	@argcheck ℭ >= 1.0

	local κup::Real = κmax(α_σ,n_σ,ℭ)
	local σ::Real = Statistics.std(f(n_σ))
	local σup::Real = ℭ*σ
	local α_1::Real = 1.0 - (1.0-α)/(1.0-α_σ)
	local κ34max::Real=κup^0.75
	n_1 = Ncb(σup/ε_a,κ34max,α_1)
	if ε_r == 0.0
		return Statistics.mean(f(n_1))
	else
		ε_1 = σup/Ncbinv(n_1,α_1,κ34max)
		local t::Integer=1
		local n_t::Integer = n_1
		local ε_t::Real = ε_1
		local ε_tm1::Real = ε_1
		while true
			local μ_t::Real = Statistics.mean(f(n_t))
			local Δ_p::Real=1.0/2.0*(tol(ε_a,ε_r*abs(μ_t-ε_t))+tol(ε_a,ε_r*abs(μ_t+ε_t)))
			if Δ_p >= ε_t
				local τ::Integer=t
				local μ_τ::Real = μ_t
				local ε_τ::Real = ε_t
				local Δ_m::Real=1.0/2.0*(tol(ε_a,ε_r*abs(μ_τ-ε_τ))-tol(ε_a,ε_r*abs(μ_τ+ε_τ)))
				return μ_τ + Δ_m
			else
				ε_tp1 = max(ε_tm1/10.0,min(ε_t/2.0,max(ε_a,θ*ε_r*abs(μ_t))))
				n_tp1 = Ncb(σup/ε_tp1,κ34max,α_t(t+1,α,α_σ))
				t=t+1
				ε_tm1 = ε_t
				ε_t = ε_tp1
				n_t = n_tp1
			end
		end
	end
end

function δ_n(n::Real,x::Real,M3::Real)
	local A1::Real = 0.3322
	local A2::Real = 0.429
	local A3::Real = 0.3031
	local A4::Real = 0.646
	local A5::Real = 0.469
	local A6::Real = 18.1139
	return 1.0/n*min(
		A1*(M3 + A2),
		A6*M3/(1.0+abs(x)^3),
		A3*(M3+A4),
		A5*M3
	)
end
function Nclt(x::Real,α::Real)
	local z::Real = Distributions.quantile(Distributions.Normal(),1.0-α/2.0)
	return ceil((z*x)^2)
end
function Ncheb(x::Real,α::Real)
	return ceil(x^2/α)
end
function be!(result::Vector,n::Real,x::Real,M::Real,α::Real)
	result[1] = Distributions.cdf(Distributions.Normal(),-sqrt(n)/x)+δ_n(n,sqrt(n)/x,M) -α/2.0
end
function Nbe(x::Real,M3::Real,α::Real)
	local N0::Integer = Ncheb(x,α)
	opt = NLopt.Opt(:LN_COBYLA, 1)
	opt.lower_bounds = [0.0]
	opt.min_objective =	(x,g) -> x
	opt.inequality_constraint = (result,n,grad) -> be!(result,n,x,M3,α)
	(minf,minx,ret) = NLopt.optimize(opt, [N0])
	return ceil(minx[1])
end
function Ncb(x::Real,κ34max::Real,α::Real)
	return min(Ncheb(x,α),Nbe(x,κ34max,α))
end
function Ncbinv(n::Real,α::Real,M::Real)
	return min(Nchebinv(n,α),Nbeinv(n,α,M))
end
function Nchebinv(n::Real,α::Real)
	return sqrt(n*α)
end
function beinv!(result::Vector,b::Real,n::Real,M::Real,α::Real)
	result[1] = Distributions.cdf(Distributions.Normal(),-sqrt(n)/b)+δ_n(n,sqrt(n)/b,M) -α/2.0
end
function Nbeinv(n::Real,α::Real,M::Real)
	local b0::Real = Nchebinv(n,α)
	opt = NLopt.Opt(:LN_COBYLA, 1)
	opt.lower_bounds = [0.0]
	opt.min_objective = (x,g) -> x
	opt.inequality_constraint = (result,n,grad) -> beinv!(result,x,n,M,α)
	(minf,minx,ret) = NLopt.optimize(opt, [b0])
	return minx[1]
end
end
